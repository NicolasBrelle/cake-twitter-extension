<?php
$this->start('title');
echo __('Signin');
$this->end();
?>

<?php if (empty($currentUser)) : ?>
    <div class="row hidden-navbar">
        <div class="form-group">

            <?php echo $this->Form->create('User', array('url' => array('action' => 'sign'), 'id' => 'form-login', 'novalidate' => 'novalidate', 'style' => 'width:100%')); ?>

            <div class="col">
                <div class="form-group">
                    <label for="login-email" class="form-label">Email</label>
                    <?php echo $this->Form->input('username', array('type' => 'text', 'label' => false, 'class' => 'form-control placeholder-color', 'id' => 'login-email', 'placeholder' => __('Your email'))); ?>
                </div>
            </div>

            <div class="col">
                <div class="form-group">
                    <label for="login-password" class="form-label">Password</label>
                    <?php echo $this->Form->input('password', array('type' => 'password', 'label' => false, 'class' => 'form-control placeholder-color', 'id' => 'login-password', 'placeholder' => __('Your password'))); ?>
                </div>
            </div>

            <div class="col">
                <div class="form-group">
                    <label for="login-password-confirm" class="form-label">Confirm Password</label>
                    <?php echo $this->Form->input('password-confirm', array('type' => 'password', 'label' => false, 'class' => 'form-control placeholder-color', 'id' => 'login-password-confirm', 'placeholder' => __('Confirm your password'))); ?>
                    <a href="<?php echo Router::url("/", true); ?>" class="text-center" style="font-size: inherit; font-size:15px;">Back to the login screen</a>
                </div>
            </div>

            <div class="col text-center">
                <button type="submit" class="btn btn-effect-ripple btn-success-custom background-stripes" style="font-size: inherit; width:50%">Sign in</button>
            </div>
        </div>
    </div>
<?php endif; ?>
