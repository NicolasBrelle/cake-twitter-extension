<?php
	$this->start("title");
	echo __("Informations");
	$this->end();

	$classAccountDiv = "d-flex col-10 offset-1";
	$classAccountInput = "col-3 offset-2";
	if (!isset($account_insta) || !isset($account_twitter)) {
		$classAccountDiv = "col-4 offset-4";
		$classAccountInput = "col-12";
	}
?>


<div class="row mb-4">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h2>
			<i class="fa fa-user" aria-hidden="true"></i>
			<?php echo __('Informations about your profil');?>
		</h2>
	</div>
</div>

<div class="row mb-4">
	<div class="d-flex col-10 offset-1" style="text-align: center; margin-bottom: 40px">
		<div class="col-3 offset-2">
			<label for="username">Email</label>
			<input id="username" type="text" value="<?php echo $username; ?>" />
		</div>
		<div class="col-3 offset-2">
			<label for="password">Change password</label>
			<input id="password" type="password" />
		</div>
		<div class="col-1">
			<label for="showPassword" style="margin-top: 38px; line-height: 0.8; font-size: 16px;">Show
				<input id="showPassword" type="checkbox" />
			</label>
		</div>
	</div>
	<div class="<?php echo $classAccountDiv; ?>" style="text-align: center;">
		<?php if (isset($account_insta)) : ?>
		<div class="<?php echo $classAccountInput; ?>">
			<label for="account_insta">Instagram Account</label>
			<input id="account_insta" type="text" value="<?php echo $account_insta; ?>" disabled/>
		</div>
		<?php endif; ?>
		<?php if (isset($account_twitter)) : ?>
		<div class="<?php echo $classAccountInput; ?>">
			<label for="account_twitter">Twitter Account</label>
			<input id="account_twitter" type="text" value="<?php echo '@'.$account_twitter; ?>" disabled/>
		</div>
		<?php endif; ?>
	</div>
    <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4 text-center mt-3">
        <button id="saveInformations" type="submit" class="btn btn-effect-ripple btn-success-custom background-stripes" style="font-size: inherit; width:100%">Save</button>
    </div>
</div>

<script>
	var ajaxSaveInfoUrl = "<?php echo Router::url("/", true); ?>users/ajaxSaveInfo";
	var usernameMissing = "<?php echo __("Username must not be empty."); ?>"
</script>

<?php if ($this->Session->read("Auth.User.stripe_id") !== null) { echo $this->element('Payments/card_list'); } ?>
<?php
echo $this->element('Subscriptions/view_subscriptions');
echo $this->Html->script("user.informations");
