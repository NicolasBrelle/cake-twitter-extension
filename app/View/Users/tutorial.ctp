<?php
$this->start('title');
echo __('Login');
$this->end();
?>

<input id="commonKey" style="display: none" value="posteria/"></input>

<div class="row" style="margin:50px">
    <div class="col-4 offset-4">
        <button id="anno-launch-tutorial" class="btn btn-effect-ripple btn-success-custom background-stripes" style="font-size: inherit; width:100%">
            <i class="fa fa-info-circle pointer animation-pulse"></i>
            <span style="margin-left: 15px;"><?php echo __('Launch tutorial') ?></span>
        </button>
    </div>
</div>

<div class="row justify-content-center tutorial" style="margin-top:50px">

    <div id="full" style="width:400px !important">
        <div class="row">
            <button class="popup-btn" name="script" action="follow_full" style="width: 58%"> <?php echo __('FOLLOW') ?> </button>
            <button class="popup-btn" id="setting1" name="settings" action="follow" style="width: 42%; float: right;">
                <i style="pointer-events: none;" class="fas fa-cog"></i>
            </button>
        </div>
        <div style="height: 5px; display: block;"></div>
        <div class="row">
            <button class="popup-btn" name="script" action="unfollow_full" style="width: 58%"> <?php echo __('UNFOLLOW') ?> </button>
            <button class="popup-btn" id="setting2" name="settings" action="unfollow" style="width: 42%; float: right;">
                <i style="pointer-events: none;" class="fas fa-cog"></i>
            </button>
        </div>
        <div style="height: 5px; display: block;"></div>
        <div class="row">
            <button class="popup-btn ignore" name="btn-stat"> <?php echo __('VIEW STATS') ?> &nbsp;&nbsp;&nbsp;<i class="fas fa-chart-bar" style="vertical-align: middle;"></i></button>
        </div>
        <?php echo __('Actions per day') ?> :
        <div id="blockSlidder" class="row">
            <div class="col-6">
                <input id="slider1" name="onchange" type="range" value=150 min=1 max=1000></input>
            </div>
            <div class="col-6">
                <input id="slider1Display" name="onchange" class="ignore" type="number" value=150 min=1 max=1000 style="width: 75px;"></input>
            </div>
        </div>
        <div id="settingsFollow" style="display: block;">
            <?php echo __('Follows per cycle') ?> :
            <div id="followPerCycleBlock" class="row">
                <div class="col-6">
                    <input style="width:95%" name="onchange" id="follow1" type="number" value=50 min=1 max=999></input> -
                </div>
                <div class="col-6">
                    <input style="width:95%" name="onchange" id="follow2" type="number" value=70 min=1 max=999></input>
                </div>
            </div>
            <?php echo __('Sleep time between cycle') ?> :
            <div id="followSleepTimePerCycle" class="row">
                <div class="col-6">
                    <input style="width:80%" name="onchange" id="sleep1" type="number" value=55 min=1 max=99></input> min -
                </div>
                <div class="col-6">
                    <input style="width:80%" name="onchange" id="sleep2" type="number" value=65 min=1 max=99></input> min
                </div>
            </div>
            <?php echo __('Like on random user') ?> :
            <div id="likeOnRandom" class="row">
                <div class="col-6">
                    <input id="likeOnMostFollowed" name="onchange" type="number" value=3 min=1 max=9></input>
                </div>
                <div class="col-6">
                    <label class="checkboxContainer margin-left-30" style="display: inline;"><?php echo __("Don't") ?>
                        <input id="/likeOnMostFollowed" name="no-limit" type="checkbox" />
                        <span id="likeOnRandomCheck" class="checkmark"></span>
                    </label>
                </div>
            </div>
        </div>
        <div id="settingsUnfollow" style="display: none;">
            <?php echo __('Unfollow per cycle') ?> :
            <div id="unfollowPerCycleBlock" class="row">
                <div class="col-6">
                    <input id="unfollowPerCycle" name="onchange" type="number" value=6 min=1 max=99></input>
                </div>
                <div class="col-6">
                    <label class="checkboxContainer margin-left-30" style="display: inline;"><?php echo __('Mass unfollow') ?>
                        <input id="/unfollowPerCycle" name="no-limit" type="checkbox" />
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <?php echo __(' Sleep time between cycle') ?> :
            <div id="unfollowSleepTimePerCycle" class="row">
                <div class="col-6">
                    <input style="width:80%" name="onchange" id="sleep3" type="number" value=8 min=1 max=99></input> min -
                </div>
                <div class="col-6">
                    <input style="width:80%" name="onchange" id="sleep4" type="number" value=10 min=1 max=99></input> min
                </div>
            </div>
            <div id="unfollowCertifiedBlock" class="row">
                <label class="checkboxContainer"><?php echo __('Unfollow certified account and people who follows you') ?>
                    <input name="forceUnfollow" type="checkbox"></input>
                    <span class="checkmark"></span>
                </label>
            </div>
            <div id="smartUnfollowBlock" class="row">
                <label class="checkboxContainer"><?php echo __('Smart unfollow (only for the follow script)') ?>
                    <input name="smartUnfollow" type="checkbox"></input>
                    <span class="checkmark"></span>
                </label>
            </div>
            <span name="daysUnfollow" style="display: none;"><?php echo __('Days Before unfollow') ?> :</span>
            <div id="daysUnfollowBlock" name="daysUnfollow" class="row" style="display: none;">
                <input name="onchange" type="number" value=3 min=1 max=9></input>
            </div>
        </div>
        <button class="popup-btn" name="logout-btn"><?php echo __('Logout') ?></button>
    </div>
</div>

<?php
    echo $this->Html->css("popup_instagram.css");
    echo $this->Html->script("popup_instagram.js");
    echo $this->Html->css("skeleton.css");

    echo $this->element('tutos');
?>
