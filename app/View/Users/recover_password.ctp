<?php
$this->start('title');
echo __('Recover password');
$this->end();
?>

<div class="row">
    <div class="form-group">

        <?php echo $this->Form->create('User', array('url' => array('action' => 'recoverPassword'), 'id' => 'form-login', 'novalidate' => 'novalidate', 'style' => 'width:100%')); ?>

        <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4">
            <div class="form-group">
                <label for="login-email" class="form-label">Email</label>
                <?php echo $this->Form->input('username', array('type' => 'text', 'label' => false, 'class' => 'form-control placeholder-color', 'id' => 'login-email', 'placeholder' => __('Your email'))); ?>
                <a href="<?php echo Router::url("/", true); ?>" style="font-size: inherit; font-size:15px">Back to the login screen</a>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4 text-center">
            <button type="submit" class="btn btn-effect-ripple btn-success background-stripes" style="font-size: inherit; width:50%">Send email</button>
        </div>
    </div>
</div>