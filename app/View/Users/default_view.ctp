<?php
$this->start('title');
echo __('Login');
$this->end();
?>

<?php if (empty($currentUser)) : ?>
    <div class="row hidden-navbar">
        <div class="form-group">

            <?php echo $this->Form->create('User', array('url' => array('action' => 'login'), 'id' => 'form-login', 'novalidate' => 'novalidate', 'style' => 'width:100%')); ?>

            <div class="col">
                <div class="form-group">
                    <label for="login-email" class="form-label">Email</label>
                    <?php echo $this->Form->input('username', array('type' => 'text', 'label' => false, 'class' => 'form-control placeholder-color', 'id' => 'login-email', 'placeholder' => __('Your email'))); ?>
                </div>
            </div>

            <div class="col">
                <div class="form-group">
                    <label for="login-password" class="form-label">Password</label>
                    <?php echo $this->Form->input('password', array('type' => 'password', 'label' => false, 'class' => 'form-control placeholder-color', 'id' => 'login-password', 'placeholder' => __('Your password'))); ?>

                    <div class="row">
                        <div class="col-6 text-center">
                            <a href="<?php echo Router::url("/", true); ?>users/recoverPassword" style="font-size: inherit; font-size:15px">Forgot your password ?</a>
                        </div>
                        <div class="col-6 text-center">
                            <a href="<?php echo Router::url("/", true); ?>users/signIn" style="font-size: inherit; font-size:15px">No account ? Sign in</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col text-center">
                <button type="submit" class="btn btn-effect-ripple btn-success-custom background-stripes" style="font-size: inherit; width:50%">Log in</button>
            </div>
        </div>
    </div>
<?php endif; ?>
