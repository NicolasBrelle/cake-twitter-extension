<?php
$this->start('title');
echo __('Change password');
$this->end();
?>

<div class="row">
    <div class="form-group">

        <?php echo $this->Form->create('User', array('url' => array('action' => "changePassword?i={$identifier}"), 'id' => 'form-login', 'novalidate' => 'novalidate', 'style' => 'width:100%')); ?>

        <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4">
            <div class="form-group">
                <label for="login-email" class="form-label"><?php echo __('Password') ?></label>
                <?php echo $this->Form->input('password', array('type' => 'text', 'label' => false, 'class' => 'form-control placeholder-color', 'placeholder' => __('Your password'))); ?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4">
            <div class="form-group">
                <label for="login-email" class="form-label"><?php echo __('Password confirmation') ?></label>
                <?php echo $this->Form->input('confirm-password', array('type' => 'text', 'label' => false, 'class' => 'form-control placeholder-color', 'placeholder' => __('Your password'))); ?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4 text-center">
            <button type="submit" class="btn btn-effect-ripple btn-success background-stripes" style="font-size: inherit; width:50%"><?php echo __('Confirm') ?></button>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4 text-center">
            <a href="<?php echo Router::url("/", true); ?>" class="btn btn-effect-ripple btn-success background-stripes" style="font-size: inherit; width:50%"><?php echo __('Cancel') ?></a>
        </div>
    </div>
</div>