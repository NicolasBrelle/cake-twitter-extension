<body>
    <div>
        <button id="callButton"><?php echo __('Join Call')?></button>
    </div>
    <div id="video-call-div">
        <video muted id="local-video" autoplay></video>
        <video id="remote-video" autoplay></video>
        <div class="call-action-div">
            <button id="muteAudioButton"><?php echo __('Mute Audio')?></button>
            <button id="muteVideoButton"><?php echo __('Hide Video')?></button>
        </div>
    </div>
</body>

<script>
    var startVideoCallUrl = '<?php echo Router::url('/', true); ?>users/ajaxVideoCall';
</script>

<?php
    echo $this->Html->css("server.call.css");
    echo $this->Html->script("receiver.js");
?>