<?php
	$this->start("title");
	echo __("Download");
	$this->end();
?>

<div class="row mb-4">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h2>
			<i class="fas fa-cloud-download-alt" aria-hidden="true"></i>
			<?php echo __('Download your plugin');?>
		</h2>
	</div>
</div>


<div class="card">
	<div class="row">
		<div class="col text-center">
			<img src="<?php echo Router::url("/", true); ?>img/Logo_twitter.png" style="height: 70px; aspect-ratio: auto; margin: 25px;">
			<form method="get" action="<?php echo Router::url("/", true); ?>Twitter_Extension.zip">
				<button type="submit" class="btn btn-effect-ripple btn-success-custom background-stripes" style="width: 100%;">Download for Twitter</button>
			</form>
		</div>
		<div class="col text-center">
			<img src="<?php echo Router::url("/", true); ?>img/Logo_Instagram.jpg" style="height: 100px; aspect-ratio: auto; margin: 10px;">
			<form method="get" action="<?php echo Router::url("/", true); ?>Instagram_Extension.zip">
				<button type="submit" class="btn btn-effect-ripple btn-success-custom background-stripes" style="width: 100%;">Download for Instagram</button>
			</form>
		</div>
	</div>
</div>
