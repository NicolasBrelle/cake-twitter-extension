<?php 
	$this->start("title");
	echo "SUBSCRIBE";
	$this->end();
?>

<!-- LOADING -->
<div id="loading" class="container" style="display: block">
    <div class="row" style="text-align: center;">
        <h2 style="margin-top: 30px">LOADING ...</h2>
    </div>
</div>

<!-- NOT LOGGED IN -->
<div id="not-logged-in" class="container" style="display: none">
    <div class="row" style="text-align: center;">
        <h2 style="margin-top: 30px">You are not logged in.</h2>
        <h4 style="margin-top: 20px">Return to your plugin and login.</h4>
    </div>
</div>

<!-- ALREADY SUBSCRIBED -->
<div id="already-subscribed" class="container" style="display: none">
    <div class="row" style="text-align: center;">
        <h2 style="margin-top: 30px">You are already subscribed for <?php echo $social; ?>.</h2>
        <h4 style="margin-top: 20px">You can return to your plugin.</h4>
    </div>
</div>

<!-- SUCCESS -->
<div id="subscribed" class="container" style="display: none">
    <div class="row" style="text-align: center;">
        <h2 style="margin-top: 30px">You are subscribed for <?php echo $social; ?> !</h2>
        <h4 style="margin-top: 20px">You can return to your plugin and enjoy the full version !</h4>
    </div>
</div>


<div id="subscribe" class="row">
    <div class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4">      
        <h2 id="title" style="text-align: center; margin-bottom: 50px; margin-top: 50px;">SUBSCRIBE</h2>
        <div id="card-element"></div>
    </div>

    <div style="margin-top:30px" class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4 text-center">
        <button id="add-card" type="button" class="btn btn-effect-ripple btn-success background-stripes" style="font-size: inherit; width:50%; display: none;">Save & Pay</button>
    </div>

    <div id="card-registered" style="display: none; margin-top:30px" class="col-xs-12 col-sm-12 col-lg-4 offset-lg-4 text-center">
        <hr style="margin-top: 10px; margin-bottom: 10px;">
        <h4 style="text-align: center;">OR</h4>
        <button id="pay-subscription" type="button" class="btn btn-effect-ripple btn-success background-stripes" style="font-size: inherit; width:50%; display: none;">Pay subscription</button>
    </div>
</div>

<script>
    var baseUrl = "<?php echo Router::url("/", true); ?>";
    var stripeKey = "<?php echo Configure::read("Stripe." . Configure::read("Stripe.mode") . "Publishable") ?>";
    var social = "<?php echo $social ?>";
    var subDiv = document.getElementById("subscribe");
    var loadDiv = document.getElementById("loading");
    var notLoggedDiv = document.getElementById("not-logged-in");
    var alreadySubDiv = document.getElementById("already-subscribed");
    var subscribedDiv = document.getElementById("subscribed");
    var registeredDiv = document.getElementById("card-registered");
</script>

<?php
    echo $this->Html->css("payment.subscribe");
    echo $this->Html->script("payment.subscribe");
    echo $this->Html->script("https://js.stripe.com/v3/");