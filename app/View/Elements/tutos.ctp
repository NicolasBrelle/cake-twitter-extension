<?php
    $next = __('Next');
    $end = __('End');
    $previous = __('Previous');
?>

<script>

    function translate_btn(anno)
    {
        $(anno.find('button')).each(function () { 
            var txt = $(this).text();
            if (txt == 'Next'){
                $(this).text('<?php echo $next ?>');
            } else if (txt == 'Previous'){
                $(this).text('<?php echo $previous ?>');
            } else if (txt == 'End'){
                $(this).text('<?php echo $end ?>');
            }
        });
    }

    function initAnnoTutorial()
    {
        if ($('#settingsFollow').is(":hidden")){
            $('#setting1').click()
        }

        let newAnno = new Anno([
            {
                target : '#setting1',
                position:'bottom',
                content:  '<?php echo __("Change the parameters of the follow feature"); ?>',
                buttons:[AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#blockSlidder',
                position:'bottom',
                content:  '<?php echo __("Number of actions to execute today"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#followPerCycleBlock',
                position:'left',
                content: '<?php echo __("Change the number of people to follow per cycle"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#followSleepTimePerCycle',
                position:'left',
                content: '<?php echo __("Change the number of waiting time between each cycle"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#likeOnRandom',
                position:'left',
                content: '<?php echo __("Allows you to like a user’s posts, in order to \"flatter\" him, this feature can be deactivated <p style=\"color:red\">SEE SPECIFIC PARAMETERS</p>"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#setting2',
                position:'bottom',
                content:  '<?php echo __("Change the parameters of the unfollow feature"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    $($target).click();
                    translate_btn($annoElem);
                },
            },
            {
                target : '#unfollowPerCycleBlock',
                position:'bottom',
                content:  '<?php echo __("Change the number of people to follow per cycle"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#unfollowSleepTimePerCycle',
                position:'bottom',
                content:  '<?php echo __("Change the number of waiting time between each cycle"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#unfollowCertifiedBlock',
                position:'bottom',
                content:  '<?php echo __("Unfollow certified account and people who follow you"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
            {
                target : '#smartUnfollowBlock',
                position:'bottom',
                content:  '<?php echo __("Allow the extension to unfollow people after following them for X days"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.NextButton],
                onShow: function (anno, $target, $annoElem) {

                    let checkbox = $($target).find('input');

                    if (!checkbox.is(":checked")){
                        checkbox.click();
                    }
                    
                    translate_btn($annoElem);
                },
            },
            {
                target : '#daysUnfollowBlock',
                position:'bottom',
                content: '<?php echo __("Number of days before allowing the extension to unfollow the user"); ?>',
                buttons:[AnnoButton.BackButton,AnnoButton.EndButton],
                onShow: function (anno, $target, $annoElem) {
                    translate_btn($annoElem);
                },
            },
        ]);

        newAnno.start();
    }

    $(document).ready(function() {
        $('#anno-launch-tutorial').click(initAnnoTutorial);
    });

</script>

<?php
    echo $this->Html->script("https://code.jquery.com/jquery-3.6.0.min.js");
    echo $this->Html->script("vendor/anno");
    echo $this->Html->script("vendor/anno");
    echo $this->Html->css("anno");