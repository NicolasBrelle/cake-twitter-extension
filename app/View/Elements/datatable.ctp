<script>
    var dataTableProcessing = '<?php echo __('Loading...'); ?>';
    var dataTableSearch = '<?php echo __('Rechercher'); ?> :';
    var dataTableLengthMenu = '<?php echo __('Show _MENU_ elements'); ?>';
    var dataTableInfo = '<?php echo __('Show element _START_ to _END_ on _TOTAL_ elements'); ?>';
    var dataTableInfoEmpty = '<?php echo __('Show element 0 to 0 on 0 element'); ?>';
    var dataTableLoading = '<?php echo __('Loading...'); ?>';
    var dataTableZeroRecords = '<?php echo __('No matching records found'); ?>';
    var dataTableEmpty = '<?php echo __('No available records to display'); ?>';
    var dataTableFirst = '<?php echo __('First'); ?>';
    var dataTablePrevious = '<?php echo __('Previous'); ?>';
    var dataTableNext = '<?php echo __('Next'); ?>';
    var dataTableLast = '<?php echo __('Last'); ?>';
</script>