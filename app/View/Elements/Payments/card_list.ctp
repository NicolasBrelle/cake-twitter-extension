<div class="row" style="margin-bottom : 50px;">

    <div class="col-12">      
        <div class="row">
            <div class="col-12 margin-bottom-10">      
                <div class="table-responsive">
                    <table id="cards-datatable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <th><?php echo __('Brand') ?></th>
                            <th><?php echo __('Number') ?></th>
                            <th><?php echo __('Exp') ?></th>
                            <th><?php echo __('Default') ?></th>
                            <th><?php echo __('Actions') ?></th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="cards-datatable-loader" style="display: none" class="col-6 offset-6">      
        <div class="row">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    </div>

</div>

<script>
    var baseUrl = "<?php echo Router::url("/", true); ?>";
</script>

<?php

echo $this->Html->script("https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js");
echo $this->Html->css("https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css");

echo $this->Html->script("payment.card_list.js");
echo $this->element('datatable');
