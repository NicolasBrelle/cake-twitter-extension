<div id="header" class="col-12 header sidebar-logo">
    <div class="" style="display: inline-block; text-align: center; color: white; padding-left: 200px;">
        <i class="far fa-clock" style="pointer-events: none;"></i>
        <span id="updateDatetime" style="padding-left: 10px;">&nbsp;</span>
    </div>

    <?php if (!empty($this->Session->read('Auth.User'))) : ?>
    <a id="logout" href="<?php echo Router::url('/', true); ?>/users/logout" style="float: right;padding-right: 20px;">
        <i class="fa fa-power-off" style="pointer-events: none;"></i>
    </a>
    <?php endif; ?>
</div>

<script>
    function persoDate(date) {
        return pad2(date.getDate()) + '/' +
            pad2(date.getMonth() + 1) + '/' +
            date.getFullYear() + ' ' +
            pad2(date.getHours()) + ':' +
            pad2(date.getMinutes()) + ':' +
            pad2(date.getSeconds());
    }

    function pad2(n) {
        return (n < 10 ? '0' : '') + n;
    }

    $(document).ready(function() {
        setInterval(function () {
            $('#updateDatetime').text(persoDate(new Date()));
        }, 1000);
    });
</script>
