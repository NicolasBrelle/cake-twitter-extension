<!--<style>-->
<!-- voir cake.generic.css -->
<!--</style>-->

<!-- Sidebar from https://codepen.io/daanvankerkom/pen/bRbKEL -->
<div id="sidebar" class="sidebar-container d-lg-block">
	<div class="sidebar-logo">
		Extension Connect
	</div>
	<ul class="sidebar-navigation">
		<li>
			<a href="<?php echo Router::url("/", true); ?>users/informations"">
				<i class="fa fa-user" aria-hidden="true"></i> <?php echo __("Profil"); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo Router::url("/", true); ?>stats/viewStats">
				<i class="fa fa-chart-bar" aria-hidden="true"></i> <?php echo __("View Statistics"); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo Router::url("/", true); ?>users/tutorial">
				<i class="fa fa-info" aria-hidden="true"></i> <?php echo __("Tutorial"); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo Router::url("/", true); ?>users/download">
				<i class="fa fa-cloud-download-alt" aria-hidden="true"></i> <?php echo __("Download"); ?>
			</a>
		</li>
		<li>
			<a href="<?php echo Router::url("/", true); ?>users/terms">
				<i class="fa fa-file-alt" aria-hidden="true"></i> <?php echo __("Terms of service"); ?>
			</a>
		</li>
		<li class="header">Actions</li>
		<li>
			<a id="logout" href="<?php echo Router::url('/', true); ?>/users/logout">
				<i class="fa fa-power-off" style="pointer-events: none;"></i> <?php echo __("Logout"); ?>
			</a>
		</li>
	</ul>
</div>
