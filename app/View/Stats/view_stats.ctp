<?php
$this->start('title');
echo __('Stats');
$this->end();
?>

<style>
	.btn-change-period {
		width:50%;
	}
</style>


<div class="row mb-4">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<h2>
			<i class="fas fa-chart-bar" aria-hidden="true"></i>
			<?php echo __('View your statistics');?>
		</h2>
	</div>
</div>

<div class="card">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
			<h3><?php echo __('Date range');?></h3>
		</div>
	</div>
	<div class="row pt-3">
		<div class="col-xs-12 col-sm-12 col-lg-3 text-center">
			<button class="btn-change-period btn btn-secondary" data-period="1w"><?php echo __('1 Week');?></button>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-3 text-center">
			<button class="btn-change-period btn btn-secondary" data-period="1m"><?php echo __('1 Month');?></button>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-3 text-center">
			<button class="btn-change-period btn btn-secondary" data-period="3m"><?php echo __('3 Month');?></button>
		</div>
		<div class="col-xs-12 col-sm-12 col-lg-3 text-center">
			<button class="btn-change-period btn btn-secondary" data-period="1y"><?php echo __('1 Year');?></button>
		</div>
	</div>
</div>

<div id="twitter_wrapper" class="card text-center">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
			<h3>Twitter</h3>
		</div>
		<div class="row pt-3">
			<p style="color: red; padding-left: 5px;"><i class="fa fa-danger"></i> <?php if(isset($needStatTwitter)) { echo __("Need to recover today stat"); } ?></p>
			<div id="stat_twitter" class="charts charts-visit col-xs-12 col-sm-12 col-lg-12"></div>
			<div id="stat_histo_twitter" class="charts charts-visit col-xs-12 col-sm-12 col-lg-12"></div>
		</div>
	</div>
</div>

<div id="insta_wrapper" class="card text-center">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
			<h3>Instagram</h3>
		</div>
		<div class="row pt-3">
			<p style="color: red; padding-left: 5px;"><i class="fa fa-danger"></i> <?php if(isset($needStatTwitter)) { echo __("Need to recover today stat"); } ?></p>
			<div id="stat_insta" class="charts charts-visit col-xs-12 col-sm-12 col-lg-12"></div>
			<div id="stat_histo_insta" class="charts charts-visit col-xs-12 col-sm-12 col-lg-12"></div>
		</div>
	</div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    var baseUrl = "<?php echo Router::url("/", true); ?>";

    if(typeof(google) !== 'undefined') {
        google.charts.load('current', {'packages':['line', 'corechart']});
    }
</script>
<?php
    echo $this->Html->script('stat.view_stat');
