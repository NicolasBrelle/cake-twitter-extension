$(document).ready(function() {
    var notyf = new Notyf({
        duration: 2000,
        position: {
          x: 'right',
          y: 'top',
        }
    });

    $("#showPassword").on("click", function(event) {
        let target = event.target;
        let passwordInput = $("#password");
    
        if (target.checked) {
            passwordInput.attr("type", "text");
            return;
        }
        passwordInput.attr("type", "password");
    });

    $("#saveInformations").on("click", function(event) {
        event.preventDefault();
        $(this).attr("disabled", true);

        if ($("#username").val().length == 0) {
            notyf.error(usernameMissing);
            return;
        }

        let dataToSave = {
            User: {
                username: $("#username").val()
            }
        };
        
        if ($("#password").val().length != 0) {
            dataToSave.User.pass = $("#password").val();
        }

        $.ajax(ajaxSaveInfoUrl, {
            method: 'POST',
            data: dataToSave,
        }).done(function (data) {
            $("#saveInformations").removeAttr("disabled");
            $("#password").val("");

            if (data.hasError) {
                notyf.error(data.msg);
                return;
            }
            notyf.success(data.msg);
        });
    });
});