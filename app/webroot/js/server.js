$('#callButton').click(startCall);

function startCall(){

    $.ajax(startVideoCallUrl, {
        method:'POST'
    }).done(function(data){
        console.log(data);
        if (data.hasError) {
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>' + data.error + '</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {
                    from: 'top',
                    amount: 80
                }
            });
        }

        const webSocket = new WebSocket("wss://127.0.0.1:8000");
        webSocket.onopen = function(e) {
            console.log("Connection established!");
        };

        webSocket.onmessage = function(e) {
            console.log(e.data);
        }; 
        

        $('#video-call-div').show();
    });
}
