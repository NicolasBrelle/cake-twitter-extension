let localStream;
let username;
let isAudio = true;
let isVideo = true;
let localvideo = document.getElementById("local-video");
let muteVideoButton = document.getElementById("muteVideoButton");
let muteAudioButton = document.getElementById("muteAudioButton");

$(document).ready(() => {
    $('#callButton').click(joinCall);
});

function sendData(data){
    const webSocket = new WebSocket("ws://192.168.1.99:8080");
    data.username = username;
    webSocket.onopen = function(e) {
         webSocket.send(JSON.stringify(data));
    };
}

function handleSignalingData(data){
    switch (data.type){
        case "offer":
            peerConn.setRemoteDescription(data.offer)
            createAndSendAnswer();
            break
        case "candidate":
            peerConn.addIceCandidate(data.candidate)
    }
}

function createAndSendAnswer(){
    peerConn.createAnswer((answer) => {
        peerConn.setLocalDescription(answer);
        sendData({
            type: "send_answer",
            answer: answer
        })
    }, error => {
        console.log(error);
    })
}

function joinCall() {
   /* 
    $.ajax(startVideoCallUrl, {
        method:'POST'
    }).done(function(data){
        if (data.hasError) {
            $.bootstrapGrowl('<h4><strong>Attention</strong></h4> <p>' + data.error + '</p>', {
                type: "danger",
                delay: 10000,
                allow_dismiss: true,
                offset: {
                    from: 'top',
                    amount: 80
                }
            });
            return;
        }
        */

        username = 'jeanPaul';

        console.log("Starting WebSocket")
        const webSocket = new WebSocket("ws://192.168.1.99:8080");
        webSocket.onopen = function(e) {
            console.log("Connection established!");
        };

        webSocket.onmessage = (event) => {
            handleSignalingData(JSON.parse(event.data))
        }

        $('#video-call-div').show();

        navigator.getUserMedia({
            video: {
                frameRate: 24,
                width: {
                    min: 480, ideal: 720, max: 1280
                },
                aspectRatio: 1.33333
            },
            audio: true
        }, (stream) => {
            localStream = stream;
            localvideo.srcObject = localStream;

            let configuration = {
                iceServers: [
                    {
                        "urls": ["stun:stun.12connect.com:3478", "stun:stun.1und1.de:3478",
                        "stun:stun.3cx.com:3478",
                        "stun:stun.acrobits.cz:3478"]
                    }
                ]
            }

            peerConn = new RTCPeerConnection(configuration)
            peerConn.addStream(localStream)

            peerConn.onaddstream = (e) => {
                localvideo.srcObject = e.stream;
            }

            peerConn.onicecandidate = ((e) => {
                if(e.candidate == null){
                    return
                }

                sendData({
                    type:"send_candidate",
                    candidate: e.candidate
                })
            })

            sendData({
                type: "join_call"
            })

            muteVideoButton.onclick = muteVideo;
            muteAudioButton.onclick = muteAudio;

        }, (error) => {
            console.log(error);
        })
    //});
}

function muteAudio(){
    isAudio = !isAudio;

    localStream.getAudioTracks()[0].enabled = isAudio;
}

function muteVideo(){
    console.log('steak');

    isVideo = !isVideo;
    localStream.getVideoTracks()[0].enabled = isVideo;
}
