$(document).ready(function(){
    refreshSubscriptions();
});

function notify(msg, status)
{
    let notyf = new Notyf({
        duration: 2000,
        position: {
          x: 'right',
          y: 'top',
        }
    });

    if (status == 'error'){
        notyf.error(msg);
    } else {
        notyf.success(msg);
    }
}

function refreshSubscriptions()
{
    $('#subscriptions-datatable tbody').empty();
    $('#subscriptions-datatable-loader').show('fast', function(){

        $.ajax(baseUrl+'users/ajaxGetSubscriptions', {
            method:"POST" 
        }).done(function (data) {
    
            console.log('data.subscriptions', data.subscriptions);
    
            if(data.hasError) {
                notify(data.error, 'error');
            } else {
                $('#subscriptions-datatable-loader').hide('fast', function(){
                    refreshSubscriptionsDatatable(data.subscriptions);
                });
            }
        });
    });
}

function refreshSubscriptionsDatatable(subscriptions)
{
    $('#subscriptions-datatable').dataTable({
        bProcessing: true,
        bServerSide: false,
        bFilter: false, 
        bInfo: false,
        paging: false,
        bSort: false,
        destroy: true,
        bAutoWidth: true,
        data: subscriptions,
        aoColumns: [
            {mData:"service"},
            {mData:"subType"},
            {mData:"date_renewal"},
            {mData:"renewal_active"},
            {mData:"created"},
            {mData:"id"},
        ],
        language: {
            processing:     dataTableProcessing,
            search:         dataTableSearch,
            lengthMenu:     dataTableLengthMenu,
            info:           dataTableInfo,
            infoEmpty:      dataTableInfoEmpty,
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: dataTableLoading,
            zeroRecords:    dataTableZeroRecords,
            emptyTable:     dataTableEmpty,
            paginate: {
                first:      dataTableFirst,
                previous:   dataTablePrevious,
                next:       dataTableNext,
                last:       dataTableLast
            }
        },
        columnDefs: [
            {
                render: function ( data, type, row ) {
                    return data;
                },
                targets: 0
            },
            {
                render: function ( data, type, row ) {
                    return data;
                },
                targets: 1
            },
            {
                render: function ( data, type, row ) {

                    if (!data){
                        return ('<div class="text-center"><i class="fa fa-times" aria-hidden="true"></i><div>');
                    }

                    return data;
                },
                targets: 2
            },
            {
                render: function ( data, type, row ) {

                    if (!data){

                        return ('<div class="btn btn-specific big-size-fa text-center" style="width:100%"><i class="fa fa-times"></i></div>');

                    } else {

                        return ('<div class="btn btn-specific big-size-fa text-center" style="width:100%"><i class="fa fa-check"></i></div>');
                    }

                },
                targets: 3
            },
            {
                render: function ( data, type, row ) {

                   return (data);
                },
                targets: 4
            },
            {
                render: function ( data, type, row ) {

                    if (!data){

                        return ('<div class="btn btn-specific big-size-fa text-center" style="width:100%" title="Upgrade subscription"><a href="'+baseUrl+'payments/subscribe?social='+row.service.toLowerCase()+'" target="_blank"><i class="fa fa-arrow-up"></i></a></div>');

                    } else {

                        let fa = 'fa fa-hand-paper';
                        let action = 'Stop renewal';

                        if (!row.renewal_active){
                            fa = 'fa fa-undo';
                            action = 'Restart renewal';
                        }

                        return ('<div class="btn btn-specific big-size-fa text-center" style="width:100%" onclick="toggleRenewal(\''+ data +'\', \''+ row.service +'\')" title="'+action+'"><i class="'+fa+'"></i></div>');
                    }
                },
                targets: 5
            }
        ]
    });
}

function toggleRenewal(subId, service)
{
    $.ajax(baseUrl+service+'Subscriptions/ajaxToggleRenewal', {
        data: {
            subId : subId
        },  
        method:"POST" 
    }).done(function (data) {
        if(data.hasError) {
            notify(data.error, 'error');
        } else {
            notify(data.msg, 'success');
            refreshSubscriptions();
        }
    });
}