function settingsHandler_full(event)
{
    var currentDiv = document.getElementById("full");

    let target = event.target;
    let action = target.getAttribute('action');

    let followSettings = currentDiv.querySelector('#settingsFollow');
    let unfollowSettings = currentDiv.querySelector('#settingsUnfollow');

    let html = document.querySelector('html');
    html.style.height = "1px";

    if (action == 'follow') {
        if (followSettings.style.display == "none") {
            followSettings.style.display = 'block';
            unfollowSettings.style.display = 'none';
        }
        else {
            followSettings.style.display = 'none';
            unfollowSettings.style.display = 'none';
        }
    } else if (action == 'unfollow') {
        if (unfollowSettings.style.display == "none") {
            followSettings.style.display = 'none';
            unfollowSettings.style.display = 'block';
        }
        else {
            followSettings.style.display = 'none';
            unfollowSettings.style.display = 'none';
        }
    }
    return true;
}

function checkboxHandler_full(event)
{
    var thisDiv = document.getElementById("full");

    let target = event.target;
    let name = target.getAttribute('name');

    if (name == 'forceUnfollow' && target.checked) {
        console.log('force');
    }
    else if (name == 'smartUnfollow') {
        let display = thisDiv.querySelectorAll('[name="daysUnfollow"]');
        let html = document.querySelector('html');
        html.style.height = "1px";

        if (target.checked) {
            for (let i = 0; i < display.length; i++) {
                display[i].style.display = 'block';
            }
        } else {
            for (let i = 0; i < display.length; i++) {
                display[i].style.display = 'none';
            }
        }
    }
}

function onchangeHandler_full(event)
{
    var currentDiv = document.getElementById("full");

    let target = event.target;

    if (target.id.includes("slider") && target.id.includes("Display")) {
        currentDiv.querySelector("#" + target.id.replace('Display', '')).value = parseInt(target.value, 10);
        return;
    }
    else if (target.id.includes("slider")) {
        currentDiv.querySelector("#" + target.id + "Display").value = parseInt(target.value, 10);
        return;
    }
    if (parseInt(target.id[target.id.length - 1], 10) % 2 == 1) {
        let secondId = target.id.substring(0, target.id.length - 1) + (parseInt(target.id[target.id.length - 1], 10) + 1);

        currentDiv.querySelector("#" + secondId).min = parseInt(target.value, 10);
        if (parseInt(currentDiv.querySelector("#" + secondId).value, 10) <=  parseInt(target.value, 10)) {
            currentDiv.querySelector("#" + secondId).value = parseInt(target.value, 10) + 1;
        }
    }
    else if (parseInt(target.id[target.id.length - 1], 10) % 2 == 0) {
        let firstId = target.id.substring(0, target.id.length - 1) + (parseInt(target.id[target.id.length - 1], 10) - 1);

        target.min = 2;
        if (parseInt(currentDiv.querySelector("#" + firstId).value, 10) >= parseInt(target.value, 10)) {
            currentDiv.querySelector("#" + firstId).value = (parseInt(target.value, 10) - 1 > 1) ? parseInt(target.value, 10) - 1 : 1;
        }
    }
}

document.addEventListener('DOMContentLoaded', function ()
{
    var currentDiv = document.getElementById("full");
    
    var settings = currentDiv.querySelectorAll('button[name="settings"]');
    settings.forEach(setting => {
        setting.addEventListener('click', settingsHandler_full);
    });

    var onchangeInputs = currentDiv.querySelectorAll('input[name="onchange"]');
    onchangeInputs.forEach(input => {
        input.addEventListener('input', onchangeHandler_full);
        input.addEventListener('keypress', (event) => {
            event.preventDefault();
        });
    });

    var checkboxInputs = currentDiv.querySelectorAll('input[type="checkbox"]');
    checkboxInputs.forEach(input => {
        input.addEventListener('click', checkboxHandler_full);
    });
});