$(document).ready(function(){
    refreshCards();
});

function notify(msg, status)
{
    let notyf = new Notyf({
        duration: 2000,
        position: {
          x: 'right',
          y: 'top',
        }
    });

    if (status == 'error'){
        notyf.error(msg);
    } else {
        notyf.success(msg);
    }
}

function refreshCards()
{
    $('#cards-datatable tbody').empty();
    $('#cards-datatable-loader').show('fast', function(){

        $.ajax(baseUrl+'payments/ajaxCardList', {
            method:"POST" 
        }).done(function (data) {
    
            console.log('data', data);
    
            if(data.hasError) {
                notify(data.error, 'error');
            } else {
                $('#cards-datatable-loader').hide('fast', function(){
                    refreshCardsDatatable(data.cards);
                });
            }
        });
    });
}

function refreshCardsDatatable(cards)
{
    $('#cards-datatable').dataTable({
        bProcessing: true,
        bServerSide: false,
        bFilter: false, 
        bInfo: false,
        paging: false,
        bSort: false,
        destroy: true,
        bAutoWidth: true,
        data: cards,
        aoColumns: [
            {mData:"brand"},
            {mData:"last4"},
            {mData:"exp"},
            {mData:"id"},
            {mData:"id"},
        ],
        language: {
            processing:     dataTableProcessing,
            search:         dataTableSearch,
            lengthMenu:     dataTableLengthMenu,
            info:           dataTableInfo,
            infoEmpty:      dataTableInfoEmpty,
            infoFiltered:   "",
            infoPostFix:    "",
            loadingRecords: dataTableLoading,
            zeroRecords:    dataTableZeroRecords,
            emptyTable:     dataTableEmpty,
            paginate: {
                first:      dataTableFirst,
                previous:   dataTablePrevious,
                next:       dataTableNext,
                last:       dataTableLast
            }
        },
        columnDefs: [
            {
                render: function ( data, type, row ) {
                    var response = '**** **** **** ' + data;
                    return response;
                },
                targets: 1
            },
            {
                render: function ( data, type, row ) {
                    if(row.default) {
                        return 'Par défaut';
                    }
                    else {
                        return '<div class="btn btn-info big-size-fa" onclick="defaultCard(\''+ data +'\')" title="Définir par défaut">Définir par défaut</div>';
                    }
                },
                targets: 3
            },
            {
                render: function ( data, type, row ) {
                    var response = '<div class="btn btn-specific big-size-fa text-center" style="width:100%" onclick="deleteCard(\''+ data +'\')" title="Supprimer"><i class="fa fa-trash"></i></div>';
                    return response;
                },
                targets: 4
            }
        ]
    });
}

function deleteCard(pmId)
{
    $.ajax(baseUrl+'payments/ajaxDeleteCard', {
        data: {
            card_id : pmId
        },  
        method:"POST" 
    }).done(function (data) {
        if(data.hasError) {
            notify(data.error, 'error');
        } else {
            notify(data.msg, 'success');
            refreshCards();
        }
    });
}

function defaultCard(pmId)
{
    $.ajax(baseUrl+'payments/ajaxDefaultCard', {
        data: {
            card_id : pmId
        },  
        method:"POST" 
    }).done(function (data) {
        if(data.hasError) {
            notify(data.error, 'error');
        } else {
            notify(data.msg, 'success');
            refreshCards();
        }
    });
}