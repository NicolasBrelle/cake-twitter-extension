var clientSecret = null,
    stripe = null,
    cardElement = null;
    request = new XMLHttpRequest();

document.addEventListener('DOMContentLoaded', function ()
{
    isSubscribed();
});

function doRequest(method, URI, data, callback)
{
    var formData = new FormData();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return;
    }
    request.send();
}

function addCard()
{
    subDiv.style.display = "none";
    loadDiv.style.display = "block";
    if (!clientSecret) {
        return;
    }

    stripe.confirmCardSetup(
        clientSecret,
        {
            payment_method: {
                card: cardElement,
                billing_details: {name: social + "-extension_customer"}
            }
        }
    ).then(function(result) {
        if (result.error) {
            console.log(result.error);
            return false;
        }
        
        let data = {
            "data[payment_method]": result.setupIntent.payment_method
        };

        doRequest("POST", "/payments/ajaxAddCard", data, function() {
            if (request.status === 200) {
                var data = JSON.parse(request.responseText);
                console.log(data);

                if (!data.hasError) {
                    doRequest("GET", "/" + social + "Subscriptions/ajaxSubscribe", null, function() {
                        if (request.status === 200) {
                            var data = JSON.parse(request.responseText);
                            console.log(data);

                            if (!data.hasError) {
                                loadDiv.style.display = "none";
                                subscribedDiv.style.display = "block";
                            }
                        }
                    });
                }
            }
        })
    });
}

function paySubscribe()
{
    loadDiv.style.display = "block";
    subDiv.style.display = "none";

    doRequest("GET", "/" + social + "Subscriptions/ajaxSubscribe", null, function() {
        if (request.status === 200) {
            var data = JSON.parse(request.responseText);
            console.log(data);

            if (!data.hasError) {
                loadDiv.style.display = "none";
                subscribedDiv.style.display = "block";
            }
        }
    });
}

function subscribe()
{
    stripe = Stripe(stripeKey);

    doRequest("GET", "/users/ajaxIsCardAdded", null, function() {
        if (request.status === 200) {
            var data = JSON.parse(request.responseText);
            console.log(data);

            loadDiv.style.display = "none";
            if (!data.hasError) {
                doRequest("GET", "/payments/ajaxGetIntent", null, function() {
                    setTimeout(function() {
                        var elements = stripe.elements();

                        cardElement = elements.create('card', {
                            'hidePostalCode' : true,
                            'style': {
                                'base': {
                                  'fontFamily': 'Arial, sans-serif',
                                  'fontSize': '40px',
                                },
                                'invalid': {
                                  'color': 'red',
                                },
                              }
                        });
                        cardElement.mount('#card-element');
                    });
                    if (request.status === 200) {
                        var data = JSON.parse(request.responseText);
                    
                        clientSecret = data.clientSecret;
                        document.getElementById("add-card").addEventListener("click", addCard);
                        document.getElementById("add-card").style.display = "";
                    }
                });
                if (!data.card) {
                    subDiv.style.display = "block";
                }
                else {
                    document.getElementById("pay-subscription").addEventListener("click", paySubscribe);
                    document.getElementById("pay-subscription").style.display = "";
                    registeredDiv.style.display = "block";
                    subDiv.style.display = "block";
                }
            }
            else {
                notLoggedDiv.style.display = "block";
            }
        }
    });
}

function isSubscribed()
{
    doRequest("GET", "/" + social + "Subscriptions/isCurrentUserSubscribed", null, function() {
        if (request.status === 200) {
            var data = JSON.parse(request.responseText);
            console.log(data);
            
            if (data.hasError) {
                subscribe();
            }
            else {
                loadDiv.style.display = "none";
                alreadySubDiv.style.display = "block";
            }
        }
    });
}