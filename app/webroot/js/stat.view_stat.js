var period = '1w';
var allStats = [];

window.addEventListener('resize', function(){
    if (typeof(google) !== 'undefined') {
        google.charts.setOnLoadCallback(processCharts);
    }
});

document.addEventListener("DOMContentLoaded", function () {
    let btnChangePeriod = document.querySelectorAll(".btn-change-period");

    btnChangePeriod.forEach(btn => {
        btn.addEventListener("click", function() {
            period = btn.getAttribute('data-period');
            processCharts();
        });
    });
    
    let serviceList = ["twitter", "insta"];
    let serviceProcessed = 0;

    serviceList.forEach((service) => {
        var response = doRequest("POST", "/stats/ajaxGetUserStats", {service: service}, function() {
            allStats[service] = false;
            serviceProcessed++;

            if (response.status === 200) {
                let data = JSON.parse(response.responseText);
                if (data.stats != undefined) {
                    allStats[service] = data.stats;
                }
            }

            if (serviceProcessed == serviceList.length) {
                if (typeof(google) !== 'undefined') {
                    google.charts.setOnLoadCallback(processCharts);
                }
            }
            return;
        });
    });
});

function processCharts() {
    var optionsLine = {
        chartArea: { width: '90%', top: '10%', bottom: '20%' },
        bar : {
            groupWidth: '100%'
        },
        legend: 'bottom',
        colors: ['#E87151', '#562ADE'],
        hAxis: {
            gridlines: { count: 7 },
        },
        vAxis: {
            gridlines: { count: 2 },
        },
        interpolateNulls: true
    };
    
    var optionsColumn = {
        chartArea: { width: '90%', top: '10%', bottom: '20%' },
        bar : {
            groupWidth: '100%'
        },
        legend: 'bottom',
        colors: ['#E87151'],
        hAxis: {
            gridlines: { count: 7 },
        },
        vAxis: {
            gridlines: { count: 2 },
        }
    };

    for (const service in allStats) {
        if (allStats[service]) {
            document.querySelector("#" + service + "_wrapper").style.display = "block";

            drawLineChart(service, allStats[service], optionsLine, [
                {name: "Follower", type: "number", statName: "follower_count"},
                {name: "Following", type: "number", statName: "following_count"}
            ]);

            drawColumnChart(service, allStats[service], optionsColumn, [
                {name: "Evolution", type: "number", statName: "follower_count"}
            ]);
        }
    }
}

function drawColumnChart(service, statArray, options, columnArray) {
    var valueCount = getDaysCount();
    var dateAndValue = createDateArray(valueCount, columnArray.length);
    var data = new google.visualization.DataTable();
    var formattedDate = null;
    var chart = null;

    chart = new google.visualization.ColumnChart(document.getElementById("stat_histo_" + service));
    chart.clearChart();
    data.addColumn('date', 'Day');

    columnArray.forEach((column) => {
        data.addColumn(column.type, column.name);
    });

    let keepLast = [];
    for (let i = 0; i < statArray.length; i++) {
        if (statArray[i].Stat.date == null) {
            continue;
        }

        formattedDate = dateToInt(statArray[i].Stat.date.replaceAll('-', '/'));

        for (let j = 0; j < dateAndValue.length; j++) {
            if (formattedDate == dateToInt(formatDate(dateAndValue[j][0]))) {
                if (keepLast.length == 0) {
                    break;
                }

                columnArray.forEach((column, index) => {
                    dateAndValue[j][index + 1] = parseInt(statArray[i].Stat[column.statName], 10) - keepLast[index];
                });
                keepLast = [];
                break;
            }
        }

        if (keepLast.length == 0) {
            columnArray.forEach((column, index) => {
                keepLast[index] = parseInt(statArray[i].Stat[column.statName], 10);
            });
        }
    }

    for (let i = 0; i < dateAndValue.length; i++) {
        let rowArray = [
            new Date(dateAndValue[i][0])
        ];

        for (let j = 1; j <= columnArray.length; j++) {
            rowArray.push(dateAndValue[i][j]);
        }

        data.addRow(rowArray);
    }

    chart.draw(data, options);
}

function drawLineChart(service, statArray, options, columnArray) {
    var valueCount = getDaysCount();
    var dateAndValue = createDateArray(valueCount, columnArray.length);
    var data = new google.visualization.DataTable();
    var formattedDate = null;
    var chart = null;

    chart = new google.visualization.LineChart(document.getElementById("stat_" + service));
    chart.clearChart();
    data.addColumn('date', 'Day');

    columnArray.forEach((column) => {
        data.addColumn(column.type, column.name);
    });

    for (let i = 0; i < statArray.length; i++) {
        if (statArray[i].Stat.date == null) {
            continue;
        }

        formattedDate = dateToInt(statArray[i].Stat.date.replaceAll('-', '/'));

        for (let j = 0; j < dateAndValue.length; j++) {
            if (formattedDate == dateToInt(formatDate(dateAndValue[j][0]))) {
                columnArray.forEach((column, index) => {
                    dateAndValue[j][index + 1] = parseInt(statArray[i].Stat[column.statName], 10);
                });
                break;
            }
        }
    }

    for (let j = 0, doNull = false; j < dateAndValue.length; j++) {
        if (dateAndValue[j][1] == 0 && dateAndValue[j][2] == 0) {
            if (doNull && j < dateAndValue.length - 1) {
                dateAndValue[j][1] = null;
                dateAndValue[j][2] = null;
            }
            continue;
        }
        doNull = true;
    }

    for (let i = 0; i < dateAndValue.length; i++) {
        let rowArray = [
            new Date(dateAndValue[i][0])
        ];

        for (let j = 1; j <= columnArray.length; j++) {
            rowArray.push(dateAndValue[i][j]);
        }

        data.addRow(rowArray);
    }

    chart.draw(data, options);
}

// --- UTILITY --- //
function doRequest(method, URI, data, callback) {
    var formData = new FormData();
    var request = new XMLHttpRequest();

    request.open(method, baseUrl + URI);
    request.onload = callback;

    if (data != null) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
        request.send(formData);
        return request;
    }
    request.send();
    return request;
}

function dateToInt(date) {
	let splitedDate = date.split('/');

	return parseInt(splitedDate[0] + splitedDate[1] + splitedDate[2], 10);
}

function formatDate(date) {
    var d = new Date(date);
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    var year = d.getFullYear();

    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }

    return [year, month, day].join('/');
}

function createDateArray(count, valueCount) {
    let date = new Date();
    let array = [];

    for (let i = 0; i < count; i++) {
        let subArray = [
            date.toDateString()
        ];

        for (let j = 1; j <= valueCount; j++) {
            subArray[j] = 0;
        }

        array.push(subArray);
        date.setDate(date.getDate() - 1);
    }
    return array;
}

function daysInThisMonth(date) {
    let daysCount = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();

    return daysCount;
}

function getDaysCount() {
    let days = 0;
    let date = new Date();

    if (period == "1w") {
        days = 7;
    }

    if (period == "1m") {
        days = daysInThisMonth(date);
    }

    if (period == "3m") {
        for (let i = 0; i < 3; i++) {
            days += daysInThisMonth(date);
            date.setMonth(date.getMonth() - 1);
        }
    }

    if (period == "1y") {
        for (let i = 0; i < 12; i++) {
            days += daysInThisMonth(date);
            date.setMonth(date.getMonth() - 1);
        }
    }

    return days;
}