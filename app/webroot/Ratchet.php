h 48
<?php
    use Ratchet\Server\IoServer;
    use Ratchet\Http\HttpServer;
    use Ratchet\WebSocket\WsServer;
    const APP = "./../";
    require APP . 'Vendor/autoload.php';
    require APP . 'Vendor/Chat.php';
    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        8080
    );
    $server->run();