<?php

require_once('Stripe/init.php');

class PaymentInterface extends AppModel {

    private $apiKey = null;
    //private $publishableKey = null;
    
    private $currency = null;

    public function __construct(){

        if (null != Configure::read('Stripe.mode') && Configure::read('Stripe.mode') == 'Live') {
            $this->apiKey = Configure::read('Stripe.LiveSecret');
        } else {
            $this->apiKey = Configure::read('Stripe.TestSecret');
        }
        
        $this->currency = Configure::read('Stripe.currency');

        \Stripe\Stripe::setApiKey($this->apiKey);
        \Stripe\Stripe::setVerifySslCerts(false);
    }

	public function paymentCreate($data) {
		try {    
            $intent = \Stripe\PaymentIntent::create([
                'amount' => intval($data['amount']*100),
                'currency' => $this->currency,
                'setup_future_usage' => 'off_session',
            ]);

            if(isset($intent->client_secret)) {
                return $intent->client_secret;
            }
            else {
                throw new Exception($ex->getMessage(), $ex->getCode());
            }
		} catch (Exception $e) {
            throw $e;
        }     
    }

    public function create_intent() {
        $intent = \Stripe\SetupIntent::create();
        return ($intent);
    }

    public function update_intent($intent_id, $customer_id) {
        $update = \Stripe\SetupIntent::update(
            $intent_id,
            ['metadata' => ['user_id' => $customer_id]]
        );
    }

    public function update_default_card($customerId, $cardId) {

        try {
            $update = \Stripe\Customer::update(
                $customerId,
                ['invoice_settings' => ['default_payment_method' => $cardId]]
            );
        } catch (Exception $e) {
            throw $e;
        } 
    }

    public function update_customer_information($customerId, $metadata, $email, $description) {
        try {
            $update = \Stripe\Customer::update(
                $customerId,
                ['metadata' => $metadata,
                'email' => $email,
                'description' => $description]
              );
            if ($update){
                return ($update);
            }
        } catch (Exception $e) {
            throw $e;
        } 
        return null;
    }

    public function get_default_pm_id($customerId) {
        try {
            $customer = $this->customerRetrieve($customerId);
            $customer = $customer->__toArray(true);

            if ($customer){
                if (isset($customer['invoice_settings']['default_payment_method'])){
                    return ($customer['invoice_settings']['default_payment_method']);
                }
                return (null);
            }

        } catch (Exception $e) {
            throw $e;
        } 
        return null;
    }

    public function getPaymentMethod($paymentMethodId)
    {
        try {

            $paymentMethod = \Stripe\PaymentMethod::retrieve($paymentMethodId);

        } catch (Exception $e) {
            throw $e;
        }

        return ($paymentMethod);
    }

    public function paymentmethodRemove($paymentMethodId) {

        try {

            $payment_method = \Stripe\PaymentMethod::retrieve($paymentMethodId);
            $payment_method->detach();

        } catch (Exception $e) {
            throw $e;
        }  

    }   

    public function paymentmethodCreate($data) {
           
        if (strpos($data['exp'], '/') !== false) {
            $exp =  explode('/', $data['exp']);
            $m = $exp[0];
            $y = $exp[1];
        } else {
            $m = substr($data['exp'], 0, 2);
            $y = substr($data['exp'], 2, 4);
        }
        
        try {

            $payement_method = \Stripe\PaymentMethod::create([
                'type' => 'card',
                'card' => [
                'number' => $data['number'],
                'exp_month' => intval($m),
                'exp_year' => intval($y),
                'cvc' => $data['cvc'],
                ],
            ]);

            if ($payement_method){
                return ($payement_method);
            } else {
                throw new Exception($ex->getMessage(), $ex->getCode());
            }

        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

	public function charge($amount, $customerId, $paymentMethod) {
		try {    

            $payment = \Stripe\PaymentIntent::create([
                'amount' => intval($amount*100),
                'currency' => $this->currency,
                'payment_method_types' => ['card'],
                'customer' => $customerId,
                'payment_method' => $paymentMethod,
                'off_session' => true,
                'confirm' => true,
            ]);

            if($payment->status == 'succeeded') {
                return $payment->id;
            }
            else {
                throw new Exception($ex->getMessage(), $ex->getCode());
            }

        } catch (Exception $e) {
            throw $e;
        } 

    }

	public function customerCreate($data) {
		try {    

            $customer = \Stripe\Customer::create($data);

            if(isset($customer->id)) {
                return $customer;
            }
            else {
                throw new Exception($ex->getMessage(), $ex->getCode());
            }

        } catch (Exception $e) {
            throw $e;
        }  
    }

	public function customerRetrieve($customerId) {
		try {    

            $customer = \Stripe\Customer::retrieve($customerId);

            if(isset($customer->id)) {
                return $customer;
            }
            else {
                throw new Exception($ex->getMessage(), $ex->getCode());
            }

        } catch (Exception $e) {
            throw $e;
        }  
    }
    
	public function customerAttachIntent($customerId, $paymentMethodId) {
		try {    

            $payment_method = \Stripe\PaymentMethod::retrieve($paymentMethodId);
            $response = $payment_method->attach(['customer' => $customerId]);

            if ($response){
                return ($response);
            }


        } catch (Exception $e) {
            throw $e;
        }
        return null;
    }

    
    public function ListCard($customerId) {

        try {    

            $list = \Stripe\PaymentMethod::all([
                'customer' => $customerId,
                'type' => 'card',
            ]);
            if (isset($list['data'])){
                return ($list['data']);
            }
            
        } catch (Exception $e) {
            throw $e;
        }  
        return null;
    }

}