<?php
require_once('TemplatesEmail.php');
require_once('CustomException.php');
require_once('Sendinblue/Mailin.php');

/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/05/2017
 * Time: 14:47
 */
class SendinBlueInterface{

    /**
     * Connection to sendingblue
     * @var type
     */
    private $connection = null;

    public function __construct($apiKey){
        $this->connection = new Mailin("https://api.sendinblue.com/v2.0", $apiKey);

        if(!is_null($this->connection)) {
            return $this->connection;
        }else{
            throw new CustomException('SendinBlueInterface/construct', SB_FAIL_CONNEXION);
        }
    }

    /**
     * Récupère les listes de contact
     * 
     */
    public function getLists($list_parent = null, $page = null, $page_limit = null){
        $param = array();
        if(!is_null($list_parent)){
            $param['list_parent'] = $list_parent;
        }
        if(!is_null($page)){
            $param['page'] = $page;
        }
        if(!is_null($page_limit)){
            $param['page_limit'] = $page_limit;
        }

        $dataList = $this->connection->get_lists($param);
        if($dataList['code'] == "success"){
            return $dataList;
        }else{
            throw new CustomException('SendinBlueInterface/getLists::'.$dataList['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Recuperer la liste des utilisateur prensent dans les liste passer en paramètre
     *
     * @param array $listIds, tableau d'identifiants des liste
     * @param null $timestamp, date minimal des résultats
     * @param null $page, numéro de la page
     * @param null $page_limit, nb element max par page
     * @return mixed
     */
    public function displayListUser($listIds = array(), $timestamp = null, $page = 1, $page_limit = 500){

        $param = array("listids" => $listIds);
        if(!is_null($page)){
            $param['page'] = $page;
        }
        if(!is_null($page_limit)){
            $param['page_limit'] = $page_limit;
        }
        if(!is_null($timestamp)){
            $param['timestamp'] = $timestamp;
        }
        $data = $this->connection->display_list_users($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            return null;
            // Return null because of display datatable function -> if throw object : internal error
            //throw new CustomException('SendinBlueInterface/displyaListUser::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    public function testValid(){
        $data = $this->connection->get_senders(null);
        if($data['code'] == "success"){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Envoi un email par le mode Transactionnel
     * (Equivalent to CakeEmail and database.php)
     * 
     */
    public function sendEmail($to, $from, $html = null, $text = null, $subject = null, $toName = null, $fromName = null){
        $param = array(
            "to" => array(
                $to => ($toName != null) ? $toName : "to email"
            ),
            "from" => array(
                $from, ($fromName != null) ? $fromName : "from email"
            )
        );
        if(!is_null($subject)){
            $param['subject'] = $subject;
        }
        if(!is_null($text)) {
            $param['text'] = $text;
        }
        if (!is_null($html)) {
            $param['html'] = $html;
        }
        $data = $this->connection->send_email($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/sendEmail::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Envoi un Template d'email par le mode Transactionnel
     * 
     */
    public function sendTemplateEmail($templateId, $to, $attr = array(), $bcc = '') {
        $param = array( 
            "id" => $templateId,
            "to" => $to,
            //"cc" => "",
            "bcc" => $bcc,
            //"replyto" => "",
            "attr" => $attr,
            //"attachment_url" => "",
            //"attachment" => array("myfilename.pdf" => "your_pdf_files_base64_encoded_chunk_data"),
            //"headers" => array("Content-Type"=> "text/html;charset=iso-8859-1", "X-param1"=> "value1")
        );

        $data = $this->connection->send_transactional_template($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/sendTemplateEmail::'.json_encode($data), SB_REQUEST_FAILURE);
        }
    }

    /**
     * Récupère les expéditeurs
     * 
     */
    public function getSenders($param = array()){
        $data = $this->connection->get_senders($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/getSenders::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Créer une campagne
     * 
     */
    public function createCampaign($listId, $subject, $from_email, $reply_to, $from_name, $html, $date, $name){

        $param = array(
            "listid" => $listId,
            "subject" => $subject,
            "from_email" => $from_email,
            "reply_to" => $reply_to,
            "from_name" => $from_name,
            "html_content"=> $html,
            "name" => $name,
            "html_url"=>"",
            "scheduled_date"=>$date
        );

        $data = $this->connection->create_campaign($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/createCampaign::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Envoi le raport d'envoi d'une campagne par email
     * 
     */
    public function campaignReportEmail($idCampaign, $subject, $to){

        $param = array(
            "id" => $idCampaign,
            "email_subject" => $subject,
            "email_to"=> $to,
            "email_content_type"=> "html",
            "email_body"=> "Please check the report of campaign id"
        );

        $data = $this->connection->campaign_report_email($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/campaignReportEmail::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Récupère une liste de campagnes
     * 
     */
    public function getCampaigns($type = null, $status = null, $page = null, $pageLimit = null){
        $param = array();

        if(!is_null($type)){
            $param['type'] = $type;
        }
        if(!is_null($status)){
            $param['status'] = $status;
        }
        if(!is_null($page)){
            $param['page'] = $page;
        }
        if(!is_null($pageLimit)){
            $param['page_limit'] = $pageLimit;
        }

        $data = $this->connection->get_campaigns_v2($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/getCampaigns::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Permet de modifier les listes d'un utilisateur
     * 
     */
    public function updateUserList($email, $listIds, $attributes = null, $listUnlinkIds = null){

        $param = array(
            "email" => $email,
            "listid" => $listIds
        );

        if($listUnlinkIds != null){
            $param['listid_unlink'] = $listUnlinkIds;
        }

        if($attributes != null){
            $param["attributes"] = $attributes;
        }

        $data = $this->connection->create_update_user($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/updateUserList::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * Permet de modifier les attributs
     * 
     */
    public function updateUserAttributes($email, $attributes) {

        $param = array(
            "email" => $email,
            "attributes" => $attributes
        );

        $data = $this->connection->create_update_user($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/updateUserAttributes::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }

    /**
     * [STATIC]
     * Permet de tracker un évènement d'automation
     * 
     */
    public static function trackEvent($email, $event){

        $datas = array(
            'email' => $email,
            'event' => $event
        );

        $head[] ='Accept: application/json';
        $head[] ='ma-key: '.Configure::read('SendinBlue.automation_key');
        $head[] ='Accept: application/json';

        $curl = curl_init();
        

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://in-automate.sendinblue.com/api/v2/trackEvent",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $head,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($datas)
        ));

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            // Windows only over-ride
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new CustomException('SendinBlueInterface/trackEvent::'.$err, SB_REQUEST_FAILURE);
        } else {
            return $response;
        }
    }
    
    public function getLeads($list = null, $page = null, $pageLimit = null) {
        $param = array();

        if(!is_null($list)){
            $param['listids'] = $list;
        }
        if(!is_null($page)){
            $param['page'] = $page;
        }
        if(!is_null($pageLimit)){
            $param['page_limit'] = $pageLimit;
        }

        $data = $this->connection->display_list_users($param);
        if($data['code'] == "success"){
            return $data;
        }else{
            throw new CustomException('SendinBlueInterface/getLeads::'.$data['message'], SB_REQUEST_FAILURE);
        }
    }
    
    /**
     * 
     * Permet de créer une liste sendinblue
     */
    public function createList($data){
        $callback = $this->connection->create_list($data);
        return $callback;
    }

    public function getUserInfo($data){
        $callback = $this->connection->get_user($data);
        return $callback;
    }

    public function getCampaignInfo($data){
        $callback = $this->connection->get_campaign_v2($data);
        return $callback;
    }

    public function getSenderInfo($data){
        $callback = $this->connection->get_senders($data);
        return $callback;
    }
    
    public function createSender($data){
        $callback = $this->connection->create_sender($data);
        return $callback;
    }

    public function getFolders($data) {
        $callback = $this->connection->get_folders($data);
        return $callback;
    }

    public function createFolder($data) {
        $callback = $this->connection->create_folder($data);
        return $callback;
    }
}