<?php
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        file_put_contents("test_WS.txt", "New connection! ({$conn->resourceId})\n", FILE_APPEND);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        file_put_contents("test_WS.txt", sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's'), FILE_APPEND);;

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        file_put_contents("test_WS.txt", "Connection {$conn->resourceId} has disconnected\n", FILE_APPEND);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        file_put_contents("test_WS.txt", "An error has occurred: {$e->getMessage()}\n", FILE_APPEND);

        $conn->close();
    }
}