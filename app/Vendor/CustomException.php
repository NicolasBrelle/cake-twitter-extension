<?php
App::uses('AppModel', 'Model');

class CustomException extends CakeException {

    public function getMessageCustom($code){
        switch($code){
            case RS_XML_MALFORMED:
                return __('RSS XML malformed');
                break;
            case RS_CONVERT_ITEM:
                return __('Fail convert XML item in Kuraotr content');
                break;
            case RS_URL_INVALID:
                return __('Invalid URL');
                break;
            case RS_UNKNOW_ERROR:
                return __('Unknow error');
                break;
            case RS_REDIRECTION_ERROR:
                return __('Error of redirection');
                break;
            default:
                return __('No Error');
                break;
        }
    }

}

//Codes TE : Twitter Exception

const TW_USER_NOT_FOUND = 10;
const TW_USER_SUSPENDED = 11;
const TW_USER_NOT_FOLLOWING = 20;
const TW_USER_BLOCKED_ME = 30;
const TW_USER_AGE = 31;
const TW_FOLLOW_IMPOSSIBLE = 40;
const TW_INVALID_TOKEN = 50;
const TW_NOT_AUTHENTICATE = 51;
const TW_MORE_5K_FRIENDS = 60;
const TW_APP_WRITE_BLOCK = 70;
const TW_MALICOUS_ACTIVITY = 80;
const TW_USERS_NOT_MATCH = 90;
const TW_RETURN_NULL = 100;
const TW_PARAMS_INVALID = 110;
const TW_NO_ACTIVE_ACCOUNT = 120;
const TW_SAVE_ERROR = 130;
const TW_DUPLICATE = 140;
const TW_ALREADY_DONE = 150;
const TW_ERROR_ACCOUNT = 160;
const TW_SET_PROCESSING = 170;

const TW_MANUAL_CANCEL = 180;

//Codes FA : FACEBOOK Exception

const FA_API_ERROR = 3000;
const FA_PARAMS_INVALID = 3010;
const FA_TOKEN_ERROR = 3020;
const FA_RESPONSE_ERROR = 3030;
const FA_ID_INVALID = 3040;
const FA_NO_ACTIVE_ACCOUNT = 3050;
const FA_SAVE_DONE = 3060;
const FA_ALREADY_DONE = 3070;
const FA_NOT_AUTHORIZED = 3080;
const FA_ERROR_ACCOUNT = 3090;
const FA_SET_PROCESSING = 3100;

//Codes LK : LINKEDIN Exception

const LK_RETURN_NULL = 4000;
const LK_ERROR_PARSE = 4010;
const LK_NOT_AUTHORIZED = 4020;
const LK_LIMIT_EXCEEDED = 4030;
const LK_NO_ACTIVE_ACCOUNT = 4040;
const LK_PARAMS_INVALID = 4050;
const LK_RESPONSE_ERROR = 4060;
const LK_SAVE_ERROR = 4070;
const LK_ALREADY_DONE = 4080;
const LK_TOKEN_EXPIRED = 4090;
const LK_ID_INVALID = 4100;
const LK_ERROR_ACCOUNT = 4110;
const LK_SET_PROCESSING = 4120;

//Codes SB : SendinBlue Exception
const SB_FAIL_CONNEXION = 6000;
const SB_REQUEST_FAILURE = 6010;

//Code CR ; Cron
const CR_FAIL_UPDATE_DATA = 7000;
const CR_FAIL_INSERT_DATA = 7010;

//code PAY: payement
const PAY_FAIL_INTENT = 15000;
const PAY_FAIL_CONNEX = 15010;
const PAY_FAIL_CHANGE_DEF = 15020;
const PAY_FAIL_CREATE_TOK = 15030;
const PAY_FAIL_CREATE_CARD = 15040;
const PAY_INTERFACE_ERROR = 15050;