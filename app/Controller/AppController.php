<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses("Controller", "Controller");

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
        "DebugKit.Toolbar",
        "Flash",
        "Acl",
        "Auth",
        "Session"
    );

    public $helpers = array("Html", "Form", "Session");

	public function beforeFilter() {

		$this->Auth->allow("adminLogin", "responseJson", "ajaxError", "ajaxSuccess");

        if(!$this->Session->check('Config.language')){
            if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $browserLanguage = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
                switch ($browserLanguage){
                    case "fr":
                        Configure::write('Config.language', 'fra');
                        $this->Session->write('Config.language', 'fra');
                        break;
                    case "es":
                        Configure::write('Config.language', 'spa');
                        $this->Session->write('Config.language', 'spa');
                        break;
                    default:
                        Configure::write('Config.language', 'eng');
                        $this->Session->write('Config.language', 'eng');
                        break;
                }
            } else {
                Configure::write('Config.language', 'eng');
                $this->Session->write('Config.language', 'eng');
            }
        } else {
            Configure::write('Config.language', $this->Session->read('Config.language'));
            $this->Session->write('Config.language', $this->Session->read('Config.language'));
        }
	}

    protected function adminLogin() {
        $admin = $this->User->find("first", array(
            "conditions" => array(
                "User.username" => "admin"
            )
        ));
        $this->Auth->login($admin);
    }

	protected function responseJSON($result) {
        $this->response->type("json");
        $this->set("result", htmlspecialchars(json_encode($result), ENT_NOQUOTES));
        $this->layout = "ajax";
        $this->render("/Elements/ajax");
    }

    protected function ajaxError($error = "An error has occured, please try again later", $other = null) {
        $result = array(
            "hasError" => true,
            "error" => $error
        );
        if ($other !== null && is_array($other)) {
            $result = array_merge($result, $other);
        }
        return $this->responseJSON($result);
    }

    protected function ajaxSuccess($success = "Success", $other = null) {
        $result = array(
            "hasError" => false,
            "msg" => $success
        );
        if ($other !== null && is_array($other)) {
            $result = array_merge($result, $other);
        }
        return $this->responseJSON($result);
    }

}
