<?php

App::uses("AppController", "Controller");

/**
 * Actions Controller
 *
 * @property Action $Action
 */
class ActionsController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("ajaxSaveAction");
    }

    public function ajaxSaveAction() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }

        $data = $this->request->data;

        if (!isset($data["user_id"]) || !isset($data["action"]) || !isset($data["count"]) || !isset($data["service"])) {
            return $this->ajaxError("Missing parameters");
        }

        $now = new DateTime();

        $this->Action->create();
        $output = $this->Action->save(array(
            "Action" => array(
                "user_id" => $data["user_id"],
                "action" => $data["action"],
                "count" => $data["count"],
				"service" => $data["service"],
                "date" => $now->format("Y-m-d H:i:s")
            )
        ));

        if (!$output) {
            return $this->ajaxError(__("Could not save action"));
        }
        return $this->ajaxSuccess(__("Action saved"));
    }
}