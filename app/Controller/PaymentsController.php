<?php

App::uses("AppController", "Controller");
App::uses("PaymentInterface", "Vendor");

/**
 * Payments Controller
 */
class PaymentsController extends AppController {
    
    public $components = array(
        "Stripe.Stripe",
        "Paginator"
    );

    function beforeFilter() {
		parent::beforeFilter();
        $this->Auth->allow('subscribe', 'ajaxGetIntent', 'ajaxCardList', 'ajaxDefaultCard', 'ajaxDeleteCard', 'ajaxAddCard');
    }

    public function subscribe()
    {
        if (!isset($_GET["social"])) {
            die();
        }
        $social = $_GET["social"];

        $this->set("social", $social);
    }

    public function ajaxGetIntent()
    {
        if ($this->request->is("GET")) {
            try {
                $PaymentInterface = new PaymentInterface();
                $intent = $PaymentInterface->create_intent();

                if (!$intent) {
                    return $this->ajaxError();
                }

                return $this->ajaxSuccess("Intent created",
                    array("clientSecret" => $intent->client_secret));
            }
            catch (Exception $ex) {
                return $this->ajaxError($ex->getMessage());
            }
        }
        return $this->ajaxError("Method not authorized");
    }

    public function ajaxAddCard()
    {
        if (!$this->request->is("POST")) {
            return $this->ajaxError("Method not authorized");
        }

        try {

            $data = $this->request->data;

            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError("Not logged in");
            }

            $pmId = $data["payment_method"];

            if (empty($pmId)) {
                return $this->ajaxError();
            }

            $this->loadModel('User');
            $user = $this->User->find("first", array(
                "fields" => array("id", "stripe_id"),
                "conditions" => array(
                    "User.id" => $this->Session->read("Auth.User.id")
                ),
                "recursive" => -1
            ));

            if (empty($user)) {
                return $this->ajaxError();
            }

            $stripeId = $user["User"]["stripe_id"];
            $PaymentInterface = new PaymentInterface();
            $response = $PaymentInterface->customerAttachIntent($stripeId, $pmId);

            if (empty($response)) {
                return $this->ajaxError();
            }

            $PaymentInterface->update_default_card($stripeId, $pmId);
            $user["User"]["card_added"] = 1;

            if (!$this->User->save($user)){
                return $this->ajaxError(__('An error occured. Please, try again.'));
            }

            return $this->ajaxSuccess("Card added");
        } catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    /**
 * [AJAX] CardList method
 *
 * @return type
 */
	public function ajaxCardList()
    {
        $result = array('hasError' => false, 'cards' => array());  
        if ($this->request->is('post')) {

            try {   
                
                $this->loadModel('User');
                $user = $this->User->findById($this->Session->read("Auth.User.id"));
                $stripeId = $user['User']['stripe_id'];

                if (!empty($user['User']['stripe_id'])){

                    $PaymentInterface = new PaymentInterface();
                    $customer = $PaymentInterface->customerRetrieve($stripeId);

                    if(!empty($customer)) {

                        $customer = $customer->__toArray(true);
                        $cards = array();
                        
                        $list_card = $PaymentInterface->ListCard($stripeId);
                
                        foreach($list_card as $card){

                            $card = $card->__toArray(true);
                            $isDefault = false;

                            if ($customer['invoice_settings']['default_payment_method'] == $card['id']){
                                $isDefault = true;
                            }

                            array_push($result['cards'], array(
                                'id' => $card['id'],
                                'last4' => $card['card']['last4'],
                                'exp' => $card['card']['exp_month'] .' / '. $card['card']['exp_year'],
                                'brand' => $card['card']['brand'],
                                'default' => $isDefault
                            ));
                        }

                    } else {
                        $result['hasError'] = true;
                        $result['error'] = __('An error occured. Please, try again.'). '[54] ';
                    }
                } else {
                    $result['hasError'] = true;
                    $result['error'] = __('An error occured. Please, try again.'). '[54] ';
                }

            } catch (Exception $ex) {
                $result['hasError'] = true;
                $result['error'] = __('An error occured. Please, try again.'). '['.$ex->getMessage().'] ';
            }
        }
        else {
            $result['hasError'] = true;
            $result['error'] = __('Method not allowed for this action');
        }
        return $this->responseJSON($result);
    }

	public function ajaxDeleteCard()
    {
        $result = array('hasError' => false);  
        if ($this->request->is('post')) {

            try {

                $this->loadModel('User');
                $user = $this->User->findById($this->Session->read("Auth.User.id"));
                $stripeId = $user['User']['stripe_id'];

                $data = $this->request->data;
                $PaymentInterface = new PaymentInterface();
                        
                if(isset($data['card_id']) && !empty($data['card_id'])) {

                    $customer_default_card = $PaymentInterface->get_default_pm_id($stripeId);
                    $update = $PaymentInterface->paymentmethodRemove($data['card_id']);
                            
                    if ($customer_default_card == $data['card_id']){

                        $card_arr = $PaymentInterface->ListCard($stripeId);
                        $card_list = array();

                        foreach($card_arr as $card){
                            $card = $card->__toArray(true);
                            array_push($card_list, array('created' => $card['created'], 'pm_id' => $card['id']));
                        }

                        if (count($card_list)){
                            function cmp($a, $b) {
                                return $a['created'] < $b['created'];
                            }
                            usort($card_list, 'cmp');
                            $PaymentInterface->update_default_card($stripeId, $card_list[0]['pm_id']);
                        }
                    }

                    $result['msg'] = __("Card deleted."); 

                } else {
                    $result['hasError'] = true;
                    $result['error'] = __('An error occured. Please, try again.'). '[00] ';
                }  
            } catch (Exception $ex) {
                $result['hasError'] = true;
                $result['error'] = __('An error occured. Please, try again.'). '[01] ';
            }
        } else {
            $result['hasError'] = true;
            $result['error'] = __('Method not allowed for this action');
        }

        return $this->responseJSON($result);
	}
        
	public function ajaxDefaultCard()
    {
        $result = array('hasError' => false);  
        if ($this->request->is('post')) {
            try {   
                
                $this->loadModel('User');
                $user = $this->User->findById($this->Session->read("Auth.User.id"));

                $stripeId = $user['User']['stripe_id'];
                $data = $this->request->data;
                
                if(isset($data['card_id']) && !empty($data['card_id'])) {  
                    
                    $PaymentInterface = new PaymentInterface();
                    $PaymentInterface->update_default_card($stripeId, $data['card_id']);               
                    
                    $result['msg'] = __("Card set by default.");

                } else {
                    $result['hasError'] = true;
                    $result['error'] = __('Error when save informations. Please, try again.')." [54]";
                }

            } catch (Exception $ex) {
                $result['hasError'] = true;
                $result['error'] = __('An error occured. Please, try again.'). '['.$ex->getMessage().'] ';
            }
        }
        else {
            $result['hasError'] = true;
            $result['error'] = __('Method not allowed for this action');
        }
        return $this->responseJSON($result);
	}

}