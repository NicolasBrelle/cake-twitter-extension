<?php

App::uses("AppController", "Controller");
App::uses("PaymentInterface", "Vendor");

/**
 * TwitterSubscriptions Controller
 *
 * @property TwitterSubscription $TwitterSubscription
 */
class TwitterSubscriptionsController extends AppController {

    private $SUB_PRICE = 19;

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("ajaxSubscribe", "isCurrentUserSubscribed", "ajaxToggleRenewal");
    }

    public function isCurrentUserSubscribed() {
        if (!$this->request->is("GET")) {
            return $this->ajaxError(__("Method not authorized"));
        }
        $this->loadModel("User");
        $this->loadModel("Stat");
        $data = $this->request->data;

        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError("Not logged in");
            }

            $user = $this->User->findById($this->Session->read("Auth.User.id"));
            $twitterSub = $user["TwitterSubscription"];
            $now = new DateTime();
            
            $doGetUserStat = $this->Stat->needGetStats($user["User"]["id"], "twitter");

            $userInfo = array(
                "user_id" => $user["User"]["id"],
                "do_get_stats" => $doGetUserStat,
                "account_twitter" => $user["User"]["account_twitter"]
            );

            if (isset($twitterSub["id"]) && 
               (isset($twitterSub["active"]) && $twitterSub["active"]) &&
               (isset($twitterSub["date_renewal"]) && $twitterSub["date_renewal"] > $now->format("Y-m-d H:i:s"))) {

                return $this->ajaxSuccess(__("User is subscribed"), $userInfo);
            }

            return $this->ajaxError("Not subscribed", $userInfo);
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxToggleRenewal() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError("Method not authorized");
        }

        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError("Not logged in");
            }

            $data = $this->request->data;
            $subscription = $this->TwitterSubscription->findById($data['subId']);

            if (empty($subscription)){
                return $this->ajaxError(__('An error occured. Please, try again.') . ' [00]');
            }

            $msg = 'Renewal stopped';
            $renewal = false;

            if (!$subscription['TwitterSubscription']['renewal_active']){

                $msg = 'Renewal restarted';
                $renewal = true;
            }

            $subscription['TwitterSubscription']['renewal_active'] = $renewal;

            if (!$this->TwitterSubscription->save($subscription)){
                return $this->ajaxError(__('An error occured. Please, try again.') . ' [01]');
            }

        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }

        return $this->ajaxSuccess($msg);
    }

    public function ajaxSubscribe() {
        if (!$this->request->is("GET")) {
            return $this->ajaxError("Method not authorized");
        }
        $this->loadModel("User");

        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError("Not logged in");
            }

            $user = $this->User->findById($this->Session->read("Auth.User.id"));
            $twitterSub = $user["TwitterSubscription"];
            $now = new DateTime();
            $inOneMonth = new DateTime();
            $inOneMonth->add(new DateInterval("P1M"));

            if (isset($twitterSub["date_renewal"]) && $twitterSub["date_renewal"] > $now->format("Y-m-d H:i:s")) {
                return $this->ajaxError("Already subscribed");
            }
            else {
                if (!$user["User"]["stripe_id"]) {
                    return $this->ajaxError();
                }

                $PaymentInterface = new PaymentInterface();
                $pmId = $PaymentInterface->get_default_pm_id($user["User"]["stripe_id"]);
                if (!$pmId) {
                    return $this->ajaxError();
                }

                $charge = $PaymentInterface->charge($this->SUB_PRICE, $user["User"]["stripe_id"], $pmId);
                if (!$charge) {
                    return $this->ajaxError();
                }
            }
            $twitterSub["user_id"] = $this->Session->read("Auth.User.id");
            $twitterSub["date_renewal"] = $inOneMonth->format("Y-m-d H:i:s");
            $twitterSub["renewal_active"] = true;
            $twitterSub["active"] = true;

            if (!$this->TwitterSubscription->save(array("TwitterSubscription" => $twitterSub))) {
                return $this->ajaxError();
            }

            return $this->ajaxSuccess("Subscribed");
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    private function createTwitterSubscription() {
        $this->loadModel("User");

        $user = $this->User->find("first", array(
            "conditions" => array(
                "User.id" => $data["user_id"]
            )
        ));

        if (!isset($user) || empty($user)) {
            throw new Exception("User does not exist", 500);
        }

        $this->TwitterSubscription->create();
        $result = $this->TwitterSubscription->save(array(
            "TwitterSubscription" => array(
                "user_id" => $data["user_id"]
            )
        ));
        if (!isset($result) || empty($result)) {
            throw new Exception("Unable to create Twitter Subscription", 500);
        }

        $twitterSub = $this->TwitterSubscription->find("first", array(
            "conditions" => array(
                "TwitterSubscription.id" => $result
            ),
            "recursive" => -1
        ));
        if (!isset($twitterSub) || empty($twitterSub)) {
            throw new Exception("Unable to create Twitter Subscription", 500);
        }

        return $twitterSub;
    }

    public function incrementSubscription() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError("Method not authorized");
        }

        try {
            $data = $this->request->data;
    
            if (!isset($data["user_id"]) || !isset($data["days"])) {
                return $this->ajaxError("Missing Parameters");
            }

            $twitterSub = $this->TwitterSubscription->find("first", array(
                "conditions" => array(
                    "TwitterSubscription.user_id" => $data["user_id"]
                ),
                "recursive" => -1
            ));

            if (!isset($twitterSub) || empty($twitterSub)) {
                $twitterSub = $this->createTwitterSubscription($data["user_id"]);
            }

            $sub = new DateTime();
            if ($twitterSub["TwitterSubscription"]["date_renewal"] > $sub) {
                $sub = new DateTime($twitterSub["TwitterSubscription"]["date_renewal"]);
            }

            $toAdd = new DateInterval("P" . $data["days"] . "D");
            $sub->add($toAdd);

            $twitterSub["TwitterSubscription"]["date_renewal"] = $sub->format("Y-m-d H:i:s");
            $result = $this->TwitterSubscription->save($twitterSub);
            if (!isset($result) || empty($result)) {
                return $this->ajaxError("An error has occured. Please contact and administrator");
            }
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }

        return $this->ajaxSuccess("Subscription updated");
    }

}