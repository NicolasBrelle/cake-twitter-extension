<?php

App::uses("AppController", "Controller");
App::uses("PaymentInterface", "Vendor");

/**
 * InstagramSubscriptions Controller
 *
 * @property InstagramSubscription $InstagramSubscription
 */
class InstagramSubscriptionsController extends AppController {

    private $SUB_PRICE = 19;

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("ajaxSubscribe", "isCurrentUserSubscribed", "ajaxToggleRenewal");
    }

    public function isCurrentUserSubscribed() {
        if (!$this->request->is("GET")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        $this->loadModel("User");
        $this->loadModel("Stat");
        $data = $this->request->data;

        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }

            $user = $this->User->findById($this->Session->read("Auth.User.id"));
            $instagramSub = $user["InstagramSubscription"];
            $now = new DateTime();

            $doGetUserStat = $this->Stat->needGetStats($user["User"]["id"], "insta");

            $userInfo = array(
                "user_id" => $user["User"]["id"],
                "do_get_stats" => $doGetUserStat,
                "account_insta" => $user["User"]["account_insta"]
            );
            
            if (isset($instagramSub["id"]) && 
               (isset($instagramSub["active"]) && $instagramSub["active"]) &&
               (isset($instagramSub["date_renewal"]) && $instagramSub["date_renewal"] > $now->format("Y-m-d H:i:s"))) {

                return $this->ajaxSuccess(__("User is subscribed"), $userInfo);
            }

            return $this->ajaxError(__("Not subscribed"), $userInfo);
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxToggleRenewal() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }

        try {

            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }

            $data = $this->request->data;
            $subscription = $this->InstagramSubscription->findById($data['subId']);

            if (empty($subscription)){
                return $this->ajaxError(__('An error occured. Please, try again.') . ' [00]');
            }

            $msg = __('Renewal stopped');
            $renewal = false;

            if (!$subscription['InstagramSubscription']['renewal_active']){

                $msg = __('Renewal restarted');
                $renewal = true;
            }

            $subscription['InstagramSubscription']['renewal_active'] = $renewal;

            if (!$this->InstagramSubscription->save($subscription)){
                return $this->ajaxError(__('An error occured. Please, try again.') . ' [01]');
            }
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }

        return $this->ajaxSuccess($msg);
    }

    public function ajaxStopRenewal() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }

        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }

            $data = $this->request->data;

            $subscription = $this->InstagramSubscription->findById($data['subId']);

            if (empty($subscription)){
                return $this->ajaxError(__('An error occured. Please, try again.') . ' [00]');
            }

            $subscription['InstagramSubscription']['renewal_active'] = 0;

            if (!$this->InstagramSubscription->save($subscription)){
                return $this->ajaxError(__('An error occured. Please, try again.') . ' [01]');
            }

        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }

        return $this->ajaxSuccess($msg);
    }

    public function ajaxSubscribe() {
        if (!$this->request->is("GET")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        $this->loadModel("User");

        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }

            $user = $this->User->findById($this->Session->read("Auth.User.id"));
            $instagramSub = $user["InstagramSubscription"];
            $now = new DateTime();
            $inOneMonth = new DateTime();
            $inOneMonth->add(new DateInterval("P1M"));

            if (isset($instagramSub["date_renewal"]) && $instagramSub["date_renewal"] > $now->format("Y-m-d H:i:s")) {
                return $this->ajaxError(__("Already subscribed"));
            }
            else {
                if (!$user["User"]["stripe_id"]) {
                    return $this->ajaxError();
                }

                $PaymentInterface = new PaymentInterface();
                $pmId = $PaymentInterface->get_default_pm_id($user["User"]["stripe_id"]);
                if (!$pmId) {
                    return $this->ajaxError();
                }

                $charge = $PaymentInterface->charge($this->SUB_PRICE, $user["User"]["stripe_id"], $pmId);
                if (!$charge) {
                    return $this->ajaxError();
                }
            }
            $instagramSub["user_id"] = $this->Session->read("Auth.User.id");
            $instagramSub["date_renewal"] = $inOneMonth->format("Y-m-d H:i:s");
            $instagramSub["renewal_active"] = true;
            $instagramSub["active"] = true;

            if (!$this->InstagramSubscription->save(array("InstagramSubscription" => $instagramSub))) {
                return $this->ajaxError();
            }

            return $this->ajaxSuccess(__("Subscribed"));
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    private function createInstagramSubscription() {
        $this->loadModel("User");

        $user = $this->User->find("first", array(
            "conditions" => array(
                "User.id" => $data["user_id"]
            )
        ));

        if (!isset($user) || empty($user)) {
            throw new Exception("User does not exist", 500);
        }

        $this->InstagramSubscription->create();
        $result = $this->InstagramSubscription->save(array(
            "InstagramSubscription" => array(
                "user_id" => $data["user_id"]
            )
        ));
        if (!isset($result) || empty($result)) {
            throw new Exception("Unable to create Instagram Subscription", 500);
        }

        $instagramSub = $this->InstagramSubscription->find("first", array(
            "conditions" => array(
                "InstagramSubscription.id" => $result
            ),
            "recursive" => -1
        ));
        if (!isset($instagramSub) || empty($instagramSub)) {
            throw new Exception("Unable to create Instagram Subscription", 500);
        }

        return $instagramSub;
    }

    public function incrementSubscription() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError("Method not authorized");
        }

        try {
            $data = $this->request->data;
    
            if (!isset($data["user_id"]) || !isset($data["days"])) {
                return $this->ajaxError("Missing Parameters");
            }

            $instagramSub = $this->InstagramSubscription->find("first", array(
                "conditions" => array(
                    "InstagramSubscription.user_id" => $data["user_id"]
                ),
                "recursive" => -1
            ));

            if (!isset($instagramSub) || empty($instagramSub)) {
                $instagramSub = $this->createInstagramSubscription($data["user_id"]);
            }

            $sub = new DateTime();
            if ($instagramSub["InstagramSubscription"]["date_renewal"] > $sub) {
                $sub = new DateTime($instagramSub["InstagramSubscription"]["date_renewal"]);
            }

            $toAdd = new DateInterval("P" . $data["days"] . "D");
            $sub->add($toAdd);

            $instagramSub["InstagramSubscription"]["date_renewal"] = $sub->format("Y-m-d H:i:s");
            $result = $this->InstagramSubscription->save($instagramSub);
            if (!isset($result) || empty($result)) {
                return $this->ajaxError("An error has occured. Please contact and administrator");
            }
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }

        return $this->ajaxSuccess("Subscription updated");
    }

}