<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

App::uses("AppController", "Controller");
App::uses("Chat","Vendor");
App::uses("PaymentInterface", "Vendor");
App::uses("SendinBlueInterface", "Vendor");

require APP . 'Vendor/autoload.php';

/**
 * Users Controller
 *
 * @property User $User
 */

class UsersController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("video_call", "video_call_receiver", "ajaxSaveLog", "changePassword", "recoverPassword", "signIn", "sign", "login", "logout", "ajaxSaveInfo", "ajaxGetCurrentUserInfo", "ajaxLogin", "ajaxLogout", "ajaxSign", "ajaxIsCardAdded", "ajaxSaveAccount", "ajaxVideoCall");
    }

    private function loadContext() {
        if (empty($this->Session->read("Auth"))) {
            return $this->redirect(
                array("controller" => "users", "action" => "defaultView")
            );
        }
    }

    private function createBothSub($userId) {
        $this->loadModel("TwitterSubscription");
        $this->loadModel("InstagramSubscription");

        $freePeriod = new DateTime();
        $freePeriod->add(new DateInterval("P5D"));
        $freePeriod = $freePeriod->format("Y-m-d H:i:s");

        $twitterSub = array(
            "TwitterSubscription" => array(
                "user_id" => $userId,
                "date_renewal" => $freePeriod,
                "renewal_active" => 1,
                "active" => 1
            )
        );

        $this->TwitterSubscription->create();
        $this->TwitterSubscription->save($twitterSub);

        $instaSub = array(
            "InstagramSubscription" => array(
                "user_id" => $userId,
                "date_renewal" => $freePeriod,
                "renewal_active" => 1,
                "active" => 1
            )
        );
        
        $this->InstagramSubscription->create();
        $this->InstagramSubscription->save($instaSub);
    }

    public function ajaxVideoCall(){
        $data = $this->request->data;

        $this->loadmodel("User");

        $user = $this->User->find("first", array(
            "conditions" => array(
                "User.id" => $this->Session->read("Auth.User.id")
            )
        ));

        if ($user == null) {
            return $this->ajaxError("User not found");
        }

        $cmd = "php ./Ratchet.php";
        $output = shell_exec($cmd);

        return $this->ajaxSuccess("Server is now listening !", array(
            "username" => $user["User"]["username"],
            "output" => $output
        ));
    }

    public function video_call(){
        $this->loadContext();
    }

    public function video_call_receiver(){
        $this->loadContext();
    }

    public function logout() {
        if (!empty($this->Session->read("Auth"))) {
            $this->Auth->logout();
        }

        return $this->redirect(
            array("controller" => "users", "action" => "defaultView")
        );
    }

    public function ajaxGetSubscriptions()
    {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }

        if (!$this->Session->read("Auth.User")) {
            return $this->ajaxError(__("Not logged in"));
        }

        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $this->Session->read("Auth.User.id")
            ),
            'contain' => array(
                'InstagramSubscription' => array(
                    'fields' => array('id', 'active', 'renewal_active', 'date_renewal', 'created'),
                    'conditions' => array(
                        'InstagramSubscription.active' => true
                    )
                ),
                'TwitterSubscription' => array(
                    'fields' => array('id', 'active', 'renewal_active', 'date_renewal', 'created'),
                    'conditions' => array(
                        'TwitterSubscription.active' => true
                    )
                )
            )
        ));

        if (empty($user)){
            return $this->ajaxError(__('Error when save informations. Please, try again.'));
        }

        $subscriptions = array();
        $socials = array('TwitterSubscription', 'InstagramSubscription');
        
        foreach($socials as $social){

            $subscription = $user[$social];
            $subscription['service'] = str_replace('Subscription', '', $social);

            if (empty($subscription['id'])){

                $subscription['active'] = true;
                $subscription['created'] = $user['User']['created'];
                $subscription['subType'] = 'lite';

            } else {

                $subscription['subType'] = 'full';
            }

            array_push($subscriptions, $subscription);
        }

        return $this->ajaxSuccess(null, array('subscriptions' => $subscriptions));
    }



    public function login() {
        if (empty($this->Session->read("Auth.User"))) {
            if (!$this->Auth->login()) {
                $this->Flash->error(__("Could not authenticate"));
                
                return $this->redirect("/");
            }
        }

        return $this->redirect(
            array("controller" => "stats", "action" => "viewStats")
        );
    }

    public function changePassword() {
        if (!isset($this->request->query['i'])){
            $this->Flash->error(__("Method not authorized"));
            return $this->redirect("/");
        }

        $this->set('identifier', $this->request->query['i']);

        if ($this->request->is('post')) {

            if (!isset($this->request->query['i'])){
                $this->Flash->error(__("An error has occured, please try again later") . ' [00]');
                return;
            }

            $identifier = $this->request->query['i'];

            $this->loadModel('Token');
            $validation = $this->Token->validateToken($identifier);

            if (!empty($validation['error'])){
                $this->Flash->error($validation['error']);
                return;
            }

            if (empty($validation['user_id'])){
                $this->Flash->error(__("An error has occured, please try again later") . ' [01]');
                return;
            }

            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $validation['user_id']
                ),
                'recursive' => -1
            ));

            if (empty($user)){
                $this->Flash->error(__("An error has occured, please try again later") . ' [02]');
                return;
            }

            $data = $this->request->data;

            if (empty($data) || !isset($data['User']['password']) || !isset($data['User']['confirm-password'])){
                $this->Flash->error(__("An error has occured, please try again later") . ' [03]');
                return;
            }

            if ($data['User']['password'] !== $data['User']['confirm-password']){
                $this->Flash->error(__("Passwords do not match"));
                return;
            }

            $newUser = array('User' => array(
                    'id' => $user['User']['id'],
                    'pass' => $data['User']['password']
                )
            );

            if (!$this->User->save($newUser)){
                $this->Flash->error(__("An error has occured, please try again later") . ' [04]');
                return;
            }

            $this->Flash->success(__('Password changed'));
            return $this->redirect("/");

        }
    }

    public function recoverPassword() {
        if ($this->request->is('post')) {
        
            $data = $this->request->data;

            if (empty($data) || !isset($data['User']['username'])){
                $this->Flash->error(__("An error has occured, please try again later") . ' [00]');

                return $this->redirect(
                    array("controller" => "users", "action" => "signIn")
                );
            }

            $this->loadModel("User");
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.username' => $data['User']['username']
                ),
                'recursive' => -1
            ));

            if (empty($user)){
                return $this->ajaxError(__("No user found with this email"));
            }

            try {
                $this->loadModel('Token');
                $token = $this->Token->createToken($user['User']['username'], $user['User']['id']);

                if (!empty($token)){

                    $sbParameters = array('KUR_LINK_MDP_RESET' => Router::url('/', true) . "users/changePassword?i={$token}");

                    $sbInterface = new SendinBlueInterface(Configure::read('SendinBlue.api_key'));
                    $sbInterface->sendTemplateEmail(TemplatesEmail::KUR_RECUP_MDP, $data['User']['username'], $sbParameters);
                }

            } catch(Exception $e){
                $this->Flash->error(__("An error has occured, please try again later") . ' [01]');
                return;
            }

            $this->Flash->success("An email has been sent");
            return $this->redirect("/");
        }
    }

    public function sign()
    {
        if (!$this->request->is('post')) {
            return $this->ajaxError(__("Method not authorized"));
        }

        $data = $this->request->data;

        if (empty($data)) {
            $this->Flash->error(__("An error has occured, please try again later"));

            return $this->redirect(
                array("controller" => "users", "action" => "signIn")
            );
        }

        if (!isset($data["User"]["username"]) || $data["User"]["username"] == "" || !isset($data["User"]["password"]) || $data["User"]["password"] == "" || !isset($data["User"]["password-confirm"])) {
            $this->Flash->error("Missing parameters");

            return $this->redirect(
                array("controller" => "users", "action" => "signIn")
            );
        }

        if ($data["User"]["password"] != $data["User"]["password-confirm"]) {
            $this->Flash->error("Passwords do not match");

            return $this->redirect(
                array("controller" => "users", "action" => "signIn")
            );
        }

        $this->loadModel("User");
        if ($this->User->checkDuplicateEntry("username", $data["User"]["username"])) {
            $this->Flash->error("Email already registered");

            return $this->redirect(
                array("controller" => "users", "action" => "signIn")
            );
        }

        $dataStripe = array(
            "email" => $data["User"]["username"]
        );

        $PaymentInterface = new PaymentInterface();
        $resultStripe = $PaymentInterface->customerCreate($dataStripe);

        if (!isset($resultStripe["id"])) {
            $this->Flash->error("An error has occured, please try again later");

            return $this->redirect(
                array("controller" => "users", "action" => "signIn")
            );
        }

        $newUser = array(
            "User" => array(
                "username" => $data["User"]["username"],
                "pass" => $data["User"]["password"],
                "stripe_id" => $resultStripe["id"],
                "subscribed" => 0,
                "date_renewal" => null
            )
        );

        $this->User->create();
        $output = $this->User->save($newUser);
        $userId = $this->User->getLastInsertID();

        if (!$output) {
            $this->Flash->error("An error has occured, please try again later");

            return $this->redirect(
                array("controller" => "users", "action" => "signIn")
            );
        }

        $this->createBothSub($userId);

        $this->Flash->success("Account created, please log in");

        return $this->redirect("/");
    }

    public function ajaxSaveAccount() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        try {
            $data = $this->request->data;

            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }

            if (!isset($data["User"]["account_insta"]) && !isset($data["User"]["account_twitter"])) {
                return $this->ajaxError(__("Missing parameters"));
            }

            $userToSave = array(
                "User" => array(
                    "id" => $this->Session->read("Auth.User.id")
                )
            );

            if (isset($data["User"]["account_insta"])) {
                $userToSave["User"]["account_insta"] = $data["User"]["account_insta"];
            }
            if (isset($data["User"]["account_twitter"])) {
                $userToSave["User"]["account_twitter"] = $data["User"]["account_twitter"];
            }

            $output = $this->User->save($userToSave);

            if (!$output) {
                return $this->ajaxError();
            }

            return $this->ajaxSuccess("Account name saved");
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxIsCardAdded() {
        if (!$this->request->is("GET")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }

            $user = $this->User->find("first", array(
                "fields" => array("card_added"),
                "conditions" => array(
                    "User.id" => $this->Session->read("Auth.User.id")
                ),
                "recursive" => -1
            ));

            if ($user["User"]["card_added"]) {
                return $this->ajaxSuccess(null,
                    array("card" => true));
            }

            return $this->ajaxSuccess(null,
                array("card" => false));
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxGetCurrentUserInfo() {
        if (!$this->request->is("GET")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        try {
            if (!$this->Session->read("Auth.User")) {
                return $this->ajaxError(__("Not logged in"));
            }
            else {
                return $this->ajaxSuccess("Info",
                    array("user" => $this->Session->read("Auth.User")));
            }
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxLogin() {
		if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        try {
            if ($this->Session->read("Auth.User")) {
                $userInfo = array(
                    "user_id" => $this->Session->read("Auth.User.id")
                );

                return $this->ajaxSuccess(__("Already logged in", $userInfo));
            }

            if (!$this->Auth->login()) {
                return $this->ajaxError(__("Could not authenticate"));
            }

            $userInfo = array(
                "user_id" => $this->Session->read("Auth.User.id")
            );

            return $this->ajaxSuccess(__("Logged in"), $userInfo);
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxLogout() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }
        try {
            if ($this->Session->read("Auth.User")) {
                $this->Auth->logout();
                
                return $this->ajaxSuccess(__("Logged out"));
            }

            return $this->ajaxError(__("Not logged in"));
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxSign() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not allowed for this action"));
        }

        $data = $this->request->data;

        try {
            if (!isset($data["User"]["username"]) || !isset($data["User"]["password"])) {
                return $this->ajaxError(__("Missing parameters"));
            }

            if (!isset($data["User"]["account_insta"]) && !isset($data["User"]["account_twitter"])) {
                return $this->ajaxError(__("Missing account"));
            }

            if ($this->User->checkDuplicateEntry("username", $data["User"]["username"])) {
                return $this->ajaxError(__("Email already registered"));
            }

            $dataStripe = array(
                "email" => $data["User"]["username"]
            );

            $PaymentInterface = new PaymentInterface();
            $resultStripe = $PaymentInterface->customerCreate($dataStripe);

            if(!isset($resultStripe["id"])) {
                return $this->ajaxError(__("Cannot create a customer, please contact an administrator"));
            }

            $newUser = array(
                "User" => array(
                    "username" => $data["User"]["username"],
                    "pass" => $data["User"]["password"],
                    "stripe_id" => $resultStripe["id"],
                    "subscribed" => 0,
                    "date_renewal" => null
                )
            );

            if (isset($data["User"]["account_twitter"])) {
                $newUser["User"]["account_twitter"] = $data["User"]["account_twitter"];
            }

            if (isset($data["User"]["account_insta"])) {
                $newUser["User"]["account_insta"] = $data["User"]["account_insta"];
            }

            $this->User->create();
            $user = $this->User->save($newUser);
            $userId = $this->User->getLastInsertID();

            if (!$user) {
                return $this->ajaxError();
            }

            $this->createBothSub($userId);

            $dataUser = $this->User->findById($user["User"]["id"]);
            $dataAuth = $dataUser["User"];

            if (!$this->Auth->login($dataAuth)) {
                return $this->ajaxError();
            }

            return $this->ajaxSuccess("Signed in",
                array("user_id" => $this->Session->read("Auth.User.id")));
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

    public function ajaxSaveInfo() {
        $this->autoRender = false;

        if (!$this->request->is("POST")) {
            return $this->ajaxError();
        }

        $userInfo = $this->request->data;
        $userInfo["User"]["id"] = $this->Session->read("Auth.User.id");

        $output = $this->User->save($userInfo);

        if (!$output) {
            return $this->ajaxError(__("An error has occured, please try again later"));
        }

        return $this->ajaxSuccess(__("Informations saved !"));
    }

    public function defaultView() {
        $currentUser = $this->Session->read('Auth.User');

        if (!empty($currentUser)) {
            return $this->redirect(
                array("controller" => "stats", "action" => "viewStats")
            );
        }
    }

    public function signIn() {
        $currentUser = $this->Session->read('Auth.User');

        if (!empty($currentUser)) {
            return $this->redirect(
                array("controller" => "stats", "action" => "viewStats")
            );
        }
    }

    public function informations() {
        $this->loadContext();

        $userId = $this->Session->read("Auth.User.id");
        $user = $this->User->getUserById($userId);

        if (isset($user["User"]["account_insta"])) {
            $this->set("account_insta", $user["User"]["account_insta"]);
        }
        if (isset($user["User"]["account_twitter"])) {
            $this->set("account_twitter", $user["User"]["account_twitter"]);
        }

        $this->set("username", $user["User"]["username"]);
    }

    public function download() {
        $this->loadContext();
    }

    public function tutorial() {
        $this->loadContext();
    }

    public function terms() {
        $this->loadContext();
    }
	
    public function ajaxSaveLog() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError(__("Method not authorized"));
        }

        try {
            $this->loadModel("Log");
			
			if (empty($this->Session->read("Auth.User"))) {
                return $this->ajaxError(__("Not logged in"));
			}
    
            $data = $this->request->data;
            if (!isset($data) || !isset($data["service"]) || !isset($data["message"])) {
                return $this->ajaxError(__("Missing parameters"));
            }

            $now = new DateTime();
            $logNow = $this->Log->find("first", array(
                "conditions" => array(
                    "Log.user_id" => $this->Session->read("Auth.User.id")
                ),
				"order" => array("Log.id DESC"),
            ));

            if (isset($logNow) && !empty($logNow)) {
				$logDate = new DateTime($logNow["Log"]["created"]); 
        		$oneMinute = new DateInterval("PT60S");
        		$now->sub($oneMinute);

				if ($now < $logDate) {
            		return $this->ajaxSuccess(__("Log already done."));
				}
            }
    
            $logToSave = array(
                "Log" => array(
                    "user_id" => $this->Session->read("Auth.User.id"),
                    "service" => $data["service"],
                    "message" => $data["message"]
                )
            );

            $this->Log->create();
            $this->Log->save($logToSave);
            return $this->ajaxSuccess(__("Log saved."));
        }
        catch (Exception $ex) {
            return $this->ajaxError($ex->getMessage());
        }
    }

}