<?php

App::uses("AppController", "Controller");

/**
 * Stats Controller
 *
 * @property Stat $Stat
 */
class StatsController extends AppController {

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("ajaxSaveStat", "ajaxGetUserStats");
    }

    public function ajaxGetUserStats() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError("Method not authorized");
        }

        $data = $this->request->data;

        if (!isset($data) || !isset($data["service"])) {
            return $this->ajaxError("Missing parameters");
        }

        $stats = $this->Stat->find("all", array(
            "conditions" => array(
                "Stat.user_id" => $this->Session->read("Auth.User.id"),
                "Stat.service" => $data["service"]
            )
        ));

        if (!$stats) {
            return $this->ajaxError("No stats");
        }
        return $this->ajaxSuccess("Stats recovered",
            array("stats" => $stats));
    }

    public function ajaxSaveStat() {
        if (!$this->request->is("POST")) {
            return $this->ajaxError("Method not authorized");
        }

        $data = $this->request->data;

        if (!isset($data) || !isset($data["user_id"]) || !isset($data["follower_count"]) || !isset($data["following_count"]) || !isset($data["service"])) {
            return $this->ajaxError("Missing parameters");
        }

        $now = new DateTime();

        $this->Stat->create();
        $output = $this->Stat->save(array(
            "Stat" => array(
                "user_id" => $data["user_id"],
                "follower_count" => $data["follower_count"],
                "following_count" => $data["following_count"],
				"service" => $data["service"],
                "date" => $now->format("Y-m-d")
            )
        ));

        if (!$output) {
            return $this->ajaxError("Could not save action");
        }
        return $this->ajaxSuccess(null);
    }

    public function viewStats() {
        $today = new DateTime();
        $today->setTime(0, 0, 0);

        $statsTodayTwitter = $this->Stat->find("first", array(
            "conditions" => array(
                "Stat.user_id" => $this->Session->read("Auth.User.id"),
                "Stat.date" => $today->format("Y-m-d"),
                "service" => "twitter"
            )
        ));
        $statsTodayInsta = $this->Stat->find("first", array(
            "conditions" => array(
                "Stat.user_id" => $this->Session->read("Auth.User.id"),
                "Stat.date" => $today->format("Y-m-d"),
                "service" => "insta"
            )
        ));

        if (empty($statsTodayTwitter)) {
            $this->set("needStatTwitter", true);
        }
        if (empty($statsTodayInsta)) {
            $this->set("needStatInsta", true);
        }
    }

}