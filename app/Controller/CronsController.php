<?php

App::uses("AppController", "Controller");
App::uses("PaymentInterface", "Vendor");

/**
 * Crons Controller
 *
 */
class CronsController extends AppController {

    private $SUB_PRICE = 19;

    function beforeFilter() {
        parent::beforeFilter();
        $this->autoRender = false;
        $this->Auth->allow("renewalSubscription", "renewalSubscriptionAsync");
    }

    public function renewalSubscription()
    {
        $this->loadModel("User");

        $now = new DateTime();
        $now->setTime(23, 59, 59);

        $userList = $this->User->find("all", array(
            "conditions" => array(
                "OR" => array(
                    array(
                        "InstagramSubscription.id !=" => null,
                        "AND" => array(
                            "InstagramSubscription.date_renewal <" => $now->format("Y-m-d H:i:s"),
                            "AND" => array(
                                "InstagramSubscription.renewal_active" => 1
                            )
                        ),
                    ),
                    array(
                        "TwitterSubscription.id !=" => null,
                        "AND" => array(
                            "TwitterSubscription.date_renewal <" => $now->format("Y-m-d H:i:s"),
                            "AND" => array(
                                "TwitterSubscription.renewal_active" => 1
                            )
                        ),
                    ),
                )
            )
        ));

        foreach ($userList as $user) {
            $userId = $user["User"]["id"];

            $url = Router::url('/', true) . "crons/renewalSubscriptionAsync/$userId";
            $cmd = "wget -bq -O /dev/null '$url'";
            
            shell_exec($cmd);
        }
    }

    private function doPayment($stripeId) {
        $PaymentInterface = new PaymentInterface();

        $pmId = $PaymentInterface->get_default_pm_id($stripeId);
        if (!$pmId) {
            return false;
        }

        $charge = $PaymentInterface->charge($this->SUB_PRICE, $stripeId, $pmId);
        if (!$charge) {
            return false;
        }

        return true;
    }

    public function renewalSubscriptionAsync($userId)
    {
        $this->loadModel("User");
        $this->loadModel("TwitterSubscription");
        $this->loadModel("InstagramSusbcription");

        $now = new DateTime();
        $now->setTime(23, 59, 59);
        $now = $now->format("Y-m-d H:i:s");

        $user = $this->User->find("first", array(
            "conditions" => array(
                "User.id" => $userId
            )
        ));

        if (!isset($user["User"]["stripe_id"])) {
            return;
        }

        if (isset($user["TwitterSubscription"]) && !empty($user["TwitterSubscription"])) {
            $twitterSub = $user["TwitterSubscription"];
        }
        if (isset($user["InstagramSusbcription"]) && !empty($user["InstagramSusbcription"])) {
            $instagramSub = $user["InstagramSusbcription"];
        }

        $stripeId = $user["User"]["stripe_id"];

        $dateNextRenewal = new DateTime();
        $dateNextRenewal->add(new DateInterval("P1M"));
        $dateNextRenewal->setTime(0, 0, 0);
        $dateNextRenewal = $dateNextRenewal->format("Y-m-d H:i:s");

        if (!empty($twitterSub) && $twitterSub["id"] != null && $twitterSub["date_renewal"] < $now && $twitterSub["renewal_active"] == 1) {
            if ($this->doPayment($stripeId)) {
                $twitterSub["date_renewal"] = $dateNextRenewal;
            }
            else {
                $twitterSub["renewal_active"] = 0;
                $twitterSub["active"] = 0;
            }

            $output = $this->TwitterSubscription->save(array(
                "TwitterSubscription" => $twitterSub
            ));
        }

        if (!empty($instagramSub) && $instagramSub["id"] != null && $instagramSub["date_renewal"] < $now && $instagramSub["renewal_active"] == 1) {
            if ($this->doPayment($stripeId)) {
                $instagramSub["date_renewal"] = $dateNextRenewal;
            }
            else {
                $instagramSub["renewal_active"] = 0;
                $instagramSub["active"] = 0;
            }

            $output = $this->InstagramSusbcription->save(array(
                "InstagramSusbcription" => $instagramSub
            ));
        }
    }
}