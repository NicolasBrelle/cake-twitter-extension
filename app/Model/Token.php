<?php

App::uses("AppModel", "Model");

/**
 * Token Model
 */
class Token extends AppModel {

    public $actsAs = array("Containable");

    public function createToken($payload, $userId) {
        $inOneHour =  new DateTime();
        $inOneHour->setTimezone(new DateTimeZone('Europe/Paris'));
        $inOneHour->add(new DateInterval("PT1H"));

        $now = new DateTime();
        $now->setTimezone(new DateTimeZone('Europe/Paris'));

        $identifier = Security::hash($payload . time(), 'sha1', true);

        $newToken = array(
            'user_id' => $userId,
            'identifier' => $identifier,
            'used' => false,
            'expire' => $inOneHour->format("Y-m-d H:i:s"),
            'created' => $now->format("Y-m-d H:i:s")
        );

        $this->create();
        if (!$this->save($newToken)){
            return (null);
        }

        return ($identifier);
    }

    public function validateToken($identifier) {
        $result = array(
            'user_id' => null,
            'error' => null
        );

        $token = $this->getByIdentifier($identifier);

        if (empty($token)){
            $result['error'] = __('Token not found');
        }

        if ($token['Token']['used'] == true){
            $result['error'] = __('Token already used');
        }

        $now =  new DateTime();
        $now->setTimezone(new DateTimeZone('Europe/Paris'));
        $expire = new DateTime($token['Token']['expire']);

        if ($expire < $now){
            $result['error'] = __('Token is expired');
        }

        $token['Token']['used'] = true;
        
        if (!$this->save($token)){
            $result['error'] = __("An error has occured, please try again later");
        }

        $result['user_id'] = $token['Token']['user_id'];

        return ($result);
    }

    public function getByIdentifier($identifier) {
        return $this->find("first", array(
            "conditions" => array(
                "identifier" => $identifier,
            )
        ));
    }

    public function getByUserId($userId) {
        return $this->find("first", array(
            "conditions" => array(
                "user_id" => $userId,
            )
        ));
    }

}