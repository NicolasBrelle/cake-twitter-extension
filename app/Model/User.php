<?php

App::uses("AppModel", "Model");
App::uses("AuthComponent", "Controller/Component");

/**
 * User Model
 */
class User extends AppModel {

    public $actsAs = array("Containable");

    public $hasOne = array(
        'TwitterSubscription' => array(
            'className' => 'TwitterSubscription',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'InstagramSubscription' => array(
            'className' => 'InstagramSubscription',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );

    public function getUserById($id) {
        $user = $this->find("first", array(
            "conditions" => array(
                "User.id" => $id
            )
        ));

        return $user;
    }

    public function beforeSave($options = array()) {  
        if (isset($this->data["User"]["pass"]) && !empty($this->data["User"]["pass"])) {
            $this->data["User"]["password"] = AuthComponent::password($this->data["User"]["pass"]);
        }
        return true;
    }

    public function checkDuplicateEntry($column, $value) {
        try {
            $exist = $this->find("first", array(
                "conditions" => array(
                    "User." . $column => $value
                ),
                "recursive" => -1
            ));

            if ($exist) {
                return true;
            }
        }
        catch (Exception $ex) {
            return false;
        }
        return false;
    }

}