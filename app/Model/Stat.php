<?php

App::uses("AppModel", "Model");

/**
 * Stat Model
 */
class Stat extends AppModel {

    public $actsAs = array("Containable");

    public function getUserStats($user_id, $days) {
        $daysAgo = new DateTime();
        
        $daysAgo->sub(new DateInterval("P${days}D"));

        $stats = $this->find("all", array(
            "conditions" => array(
                "user_id" => $user_id,
                "date >" => $daysAgo->format("Y-m-d")
            ),
            "order" => "date ASC"
        ));

        return $stats;
    }

    public function needGetStats($userId, $service) {
        $oneDayAgo =  new DateTime();
        
        $oneDayAgo->sub(new DateInterval("P1D"));

        $stats = $this->find("first", array(
            "conditions" => array(
                "user_id" => $userId,
                "service" => $service,
                "date >" => $oneDayAgo->format("Y-m-d")
            )
        ));

        return (!isset($stats) || empty($stats));
    }

}