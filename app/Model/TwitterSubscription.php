<?php

App::uses("AppModel", "Model");

/**
 * TwitterSubscription Model
 */
class TwitterSubscription extends AppModel {

    public $actsAs = array("Containable");

    public function checkDuplicateEntry($column, $value) {
        try {
            $exist = $this->find("first", array(
                "conditions" => array(
                    "TwitterSubscription." . $column => $value
                )
            ));

            if ($exist) {
                return true;
            }
        }
        catch (Exception $ex) {
            return false;
        }
        return false;
    }

}