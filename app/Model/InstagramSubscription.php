<?php

App::uses("AppModel", "Model");

/**
 * InstagramSubscription Model
 */
class InstagramSubscription extends AppModel {

    public $actsAs = array("Containable");

    public function checkDuplicateEntry($column, $value) {
        try {
            $exist = $this->find("first", array(
                "conditions" => array(
                    "InstagramSubscription." . $column => $value
                )
            ));

            if ($exist) {
                return true;
            }
        }
        catch (Exception $ex) {
            return false;
        }
        return false;
    }

}